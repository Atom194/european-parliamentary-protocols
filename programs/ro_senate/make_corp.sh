#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp ro_senate.reg /corpora/registry/ro_senate

cat data/downloaded/* | $(ca_getpipeline ro) | 7zpipe /corpora/vert/parliament/ro_senate.vert.7z
compilecorp ro_senate --recompile-corpus
../deploy_to_beta.sh ro_senate