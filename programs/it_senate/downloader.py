# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

remaining_urls = ["https://www.senato.it/static/bgt/listaresaula/18/2018/index.html?static=true"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.senato.it/static/bgt/listaresaula/"):
        response = requests.get(url, timeout=1200)
        if response.status_code == 404:
            return None
        
        legis, year = re.sub(r'https://www.senato.it/static/bgt/listaresaula/([0-9]+)/([0-9]+)/index.html\?static=true', "\\1 \\2", url).split(" ")
        year = int(year)
        legis = int(legis)
        
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and element.get("href").startswith("http://www.senato.it/japp/bgt/showdoc/frame.jsp?tipodoc=Resaula"):
                id = int(re.sub(r'http://www.senato.it/japp/bgt/showdoc/frame.jsp\?tipodoc=Resaula&leg=[0-9]+&id=([0-9]+)', "\\1", element.get("href")))
                remaining_urls.append("https://www.senato.it/japp/bgt/showdoc/REST/v1/showdoc/get/fragment/%s/Resaula/0/%s/doc_dc-ressten_rs" % (legis, id))
        remaining_urls.append("https://www.senato.it/static/bgt/listaresaula/%s/%s/index.html?static=true" % (legis, year + 1))
        remaining_urls.append("https://www.senato.it/static/bgt/listaresaula/%s/%s/index.html?static=true" % (legis + 1, year))
    if url.startswith("https://www.senato.it/japp/bgt/showdoc/REST/v1/showdoc/get/fragment/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, allow_redirects=True, timeout=1200)
        filename = re.sub(r'https://www.senato.it/japp/bgt/showdoc/REST/v1/showdoc/get/fragment/([0-9]+)/Resaula/0/([0-9]+)/doc_dc-ressten_rs', "\\1_\\2.prevert", url)
        url_metadata = re.sub(r'/doc_dc-ressten_rs$', "", url)
        url_metadata = re.sub(r'/get/fragment/', '/get/metadata/', url_metadata)
        try:
            metadata_json = requests.get(url_metadata, timeout=1200).json()
            date = re.sub(r'.*([0-9]{2})[/]([0-9]{2})[/]([0-9]{4}).*', "\\3-\\2-\\1", metadata_json["titoloAtto"])
            year, month, day = date.split("-")
            year = int(year)
            month = int(month)
            day = int(day)
        except:
            year = "===NONE==="
            month = "===NONE==="
            day = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")
    #parser.add_argument("-d", "--dir", default="data", help="Directory where downloaded data is stored.")
    #parser.add_argument("-c", "--cleandata", help="Downloaded data are stored after applying get_metadata function.")
    #parser.add_argument("-s", "--cleandata", help="Periodicly checked addresses for new URLs to download from.")

    args = parser.parse_args()
    run(args)

