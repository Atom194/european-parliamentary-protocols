# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime

month_table = {
    "janvier": 1,
    "février": 2,
    "fevrier": 2,
    "mars": 3,
    "avril": 4,
    "mai": 5,
    "juin": 6,
    "juillet": 7,
    "août": 8,
    "aout": 8,
    "septembre": 9,
    "octobre": 10,
    "novembre": 11,
    "décembre": 12,
    "januari": 1,
    "februari": 2,
    "maart": 3,
    "april": 4,
    "mei": 5,
    "juni": 6,
    "juli": 7,
    "augustus": 8,
    "september": 9,
    "oktober": 10,
    "november": 11,
    "december": 12,
}

remaining_urls = ["https://www.tweedekamer.nl/kamerstukken/plenaire_verslagen?qry=%2A&cfg=tksearch&fld_tk_categorie=Kamerstukken&fld_prl_kamerstuk=Plenaire%20verslagen&srt=date%3Adesc%3Adate&sta=0"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.tweedekamer.nl/kamerstukken/plenaire_verslagen?"):
        response = requests.get(url, verify=False, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        for element in elements:
            if "Volgende pagina" in element.text:
                offset = re.sub(r'.*&sta=([0-9]+).*', "\\1", element.get("href"))
                if int(offset) <= 200:
                    remaining_urls.append("https://www.tweedekamer.nl/kamerstukken/plenaire_verslagen" + element.get("href"))
            elif element.get("href") is not None and element.get("href").startswith("/kamerstukken/plenaire_verslagen/detail/"):
                remaining_urls.append("https://www.tweedekamer.nl" + element.get("href"))
    elif url.startswith("https://www.tweedekamer.nl/kamerstukken/plenaire_verslagen/detail/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, verify=False, timeout=1200)
        filename = re.sub(r'https://www.tweedekamer.nl/kamerstukken/plenaire_verslagen/detail/([^/]+)/([^/]+)', "\\1_\\2", url) + ".prevert"

        date_match = re.search(r' [0-9]{1,2} [A-Za-z(&auml;)äû]+ [0-9]{4}', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r' ([0-9]{1,2}) ([A-Za-z(&auml;)äû]+) ([0-9]{4})', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

