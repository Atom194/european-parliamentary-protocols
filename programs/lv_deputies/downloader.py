# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

TIMEOUT = 1200

month_table = {
    "janvāris": 1,
    "janvāra": 1,
    "janvārī": 1,
    "februāris": 2,
    "februāra": 2,
    "februārī": 2,
    "marts": 3,
    "marta": 3,
    "martā": 3,
    "aprīlis": 4,
    "aprīļa": 4,
    "aprīlī": 4,
    "maijs": 5,
    "maija": 5,
    "maijā": 5,
    "jūnijs": 6,
    "jūnijā": 6,
    "jūnija": 6,
    "jūlijs": 7,
    "jūlija": 7,
    "jūlijā": 7,
    "augusts": 8,
    "augusta": 8,
    "augustā": 8,
    "septembris": 9,
    "septembra": 9,
    "septembrī": 9,
    "oktobris": 10,
    "oktobra": 10,
    "oktobrī": 10,
    "novembris": 11,
    "novembra": 11,
    "novembrī": 11,
    "novembri": 11,
    "decembris": 12,
    "decembra": 12,
    "decembrī": 12,
}

remaining_urls = ["https://www.saeima.lv/lv/transcripts"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url == "https://www.saeima.lv/lv/transcripts":
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and element.get("href").startswith("/lv/transcripts/category/"):
                remaining_urls.append("https://www.saeima.lv" + element.get("href"))
    if url.startswith("https://www.saeima.lv/lv/transcripts/category/"):
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and element.get("href").startswith("/lv/transcripts/view/"):
                remaining_urls.insert(0, "https://www.saeima.lv" + element.get("href"))
    if url.startswith("https://www.saeima.lv/lv/transcripts/view/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        page_text = BeautifulSoup(response.text, "html.parser").text
        filename = url.split("/")[-1] + ".prevert"

        date_match = re.search(r'[0-9]{4}\.\s*gada\s*[0-9]{1,2}\s*[0-9\.,\sun]*\.\s*[A-Za-zāīļū]+', page_text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = page_text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'([0-9]{4})\.\s*gada\s*([0-9]{1,2})[0-9\.,\sun]*\.\s*([A-Za-zāīļū]+)', "\\2 \\3 \\1", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    if "time_out" in config:
        global TIMEOUT
        TIMEOUT = config["time_out"]
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

