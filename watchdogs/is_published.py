#!/bin/python3
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

# USAGE:
# python is_published.py <bonito_url> <corpus_name> <days_before_warning> <days_before_critical>
# python is_published.py "https://beta.sketchengine.eu/bonito/run.cgi" "be_deputies" 10 20

API_KEY = ""

import requests
from datetime import date, timedelta
import sys

LOGIN_URL = "https://app.sketchengine.eu/ca/api/session"

TARGET_URL = sys.argv[1]
CORPUS_NAME = sys.argv[2]
WARNING_DAYS = int(sys.argv[3])
CRITICAL_DAYS = int(sys.argv[4])

TODAY = date.today()
PREW_MONTH = TODAY.replace(day=1) - timedelta(days=1)
SKE_URL = TARGET_URL + '/attr_vals?corpname=preloaded/' + CORPUS_NAME + '&avattr=doc.date&avpat=' + TODAY.strftime("%Y-%m") + '-.*|' + PREW_MONTH.strftime("%Y-%m") + '-.*&avmaxitems=1000&format=json'

with requests.Session() as session:
    response_json = session.get(SKE_URL, headers={"Authorization": "Bearer " + API_KEY}).json()
    response_json['suggestions'].sort(reverse=True)
    if len(response_json['suggestions']) == 0:
        print("CRITICAL: No data in past two months.")
        exit(2)
    last_date = date.fromisoformat(response_json['suggestions'][0])
    
    if last_date > TODAY - timedelta(days=WARNING_DAYS):
        print("OK: Last protocol from: " + last_date.strftime("%Y-%m-%d"))
        exit(0)
    if last_date > TODAY - timedelta(days=CRITICAL_DAYS):
        print("WARNING: Last protocol from: " + last_date.strftime("%Y-%m-%d"))
        exit(1)
    print("CRITICAL: Last protocol from: " + last_date.strftime("%Y-%m-%d"))
    exit(2)