# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def _write_rec(self, node: Element):
        if node.tag == "nominativo":
            if self.last_speaker is not None:
                self.file.write("</speaker>\n")
            self.last_speaker = node.attrib["cognomeNome"]
            self.file.write('<speaker name="%s" role="%s">\n' % (self.last_speaker, node.text))
        elif node.tag == "interventoVirtuale":
            if node.text is not None and len(node.text.strip()) > 0:
                self.file.write("<p>\n" + node.text.strip() + "\n</p>\n")
        elif node.tag == "emphasis":
            if node.text is not None and len(node.text.strip()) > 0:
                note = node.text.strip()
                if note.startswith("(") and note.endswith(")"):
                    self.file.write('<note type="other">\n' + note + "\n</note>\n")
                else:
                    self.file.write("<p>\n" + note + "\n</p>\n")
        elif node.tag == "testoXHTML":
            if node.text is not None and len(node.text.strip()) > 0:
                self.file.write("<p>\n" + node.text.strip() + "\n</p>\n")

        if node.tail is not None:
            tail = node.tail
            tail = re.sub(r'^\.', "", tail)
            tail = tail.strip()
            if len(tail) > 0:
                self.file.write("<p>\n" + tail + "\n</p>\n")

        for child in node:
            self._write_rec(child)


    def write(self, xml_file: IO) -> None:
        stenoprotocol_xml = ET.parse(xml_file)
        self._write_rec(stenoprotocol_xml.getroot())
    

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
