# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<a href="#">[^<]*</a>[:][ ]*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.is_final_document = True
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        if not self.is_final_document:
            return

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
            elif element.name == "head":
                continue
            elif element.name == "p":
                if [x.name for x in element.parents] == ["p", "body", "html", "[document]"] and element.has_attr("align") and (element.attrs["align"] == "justify" or element.attrs["align"] == "center"):
                    element_str = str(element)
                    new_speaker = None
                    new_speaker_pos = re.search(re_speaker, element_str)
                    if new_speaker_pos is not None:
                        new_speaker = element_str[new_speaker_pos.start():new_speaker_pos.end()]
                        new_speaker = re.sub('<a[^>]*>', '', new_speaker)
                        new_speaker = re.sub('</a>[: ]*', '', new_speaker)
                        element_str = element_str[:new_speaker_pos.start()] + element_str[new_speaker_pos.end():]
                        if self.last_speaker is not None:
                            self.file.write("</speaker>\n")
                        self.file.write('<speaker name="%s">\n' % (new_speaker))
                        self.last_speaker = new_speaker
                    element_str = re.sub('<p[^>]*>', '', element_str)
                    element_str = re.sub('</p>', '', element_str)
                    element_str = re.sub('<b>', '', element_str)
                    element_str = re.sub('</b>', '', element_str)
                    element_str = re.sub('<br/>', '', element_str)
                    element_str = re.sub('<br>', '', element_str)
                    element_str = element_str.strip()
                    if len(element_str) != 0:
                        self.file.write("<p>\n%s\n</p>\n" % (element_str.strip()))
                else:
                    self._write_rec(element)
            elif element.name == "br":
                if [x.name for x in element.parents] == ["body", "html", "[document]"]:
                    continue
                elif [x.name for x in element.parents] == ["p", "body", "html", "[document]"]:
                    continue
                else:
                    unchatched_element(element)
            elif element.name == "center":
                continue
            elif element.name == "a":
                continue
            elif element.name == "font":
                if element.get("color") is not None and element.get("color") == '#ff0000' and "Neprošlo jazykovou korekturou" in element.text:
                    self.is_final_document = False
                    return
            else:
                unchatched_element(element)

    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read().decode("windows-1250"), "html.parser")
        self._write_rec(page)
    
    def build(self):
        if not self.is_final_document:
            self.file.close()
            import os
            os.remove(self.crawler.dir_unconfirmed + "/" + self.filename)
            raise AssertionError("This document is not corected. Try again later.")
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)