#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp bg_deputies.reg /corpora/registry/bg_deputies

cat data/downloaded/* | $(ca_getpipeline bg) | 7zpipe /corpora/vert/parliament/bg_deputies.vert.7z
compilecorp bg_deputies --recompile-corpus
../deploy_to_beta.sh bg_deputies