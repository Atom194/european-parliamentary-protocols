# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'[^0-9]+: ?[—-]')
re_speaker2 = re.compile(r'[^0-9]+\):')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, text_file: IO) -> None:
        for line in text_file.readlines():
            line = re.sub(r'\s+', " ", line)
            line = line.strip()
            speaker_match = re.search(re_speaker, line)
            if not (speaker_match is not None and speaker_match.start() == 0):
                speaker_match = re.search(re_speaker2, line)
            if speaker_match is not None and speaker_match.start() == 0:
                if self.last_speaker is not None:
                    self.file.write("</speaker>\n")
                self.last_speaker = line[:speaker_match.end()][:-1]
                self.last_speaker = self.last_speaker.replace(":", "")
                self.last_speaker = self.last_speaker.strip()
                line = line[speaker_match.end():]
                self.file.write('<speaker name="%s">\n' % self.last_speaker)
            
            line = re.sub(r'\s+', " ", line)
            line = line.strip()
            if len(line) > 0:
                self.file.write("<p>\n" + line + "\n</p>\n")
                

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
