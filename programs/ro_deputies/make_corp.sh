#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp ro_deputies.reg /corpora/registry/ro_deputies

cat data/downloaded/* | $(ca_getpipeline ro) | 7zpipe /corpora/vert/parliament/ro_deputies.vert.7z
compilecorp ro_deputies --recompile-corpus
../deploy_to_beta.sh ro_deputies