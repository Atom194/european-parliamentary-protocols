#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp ee_deputies.reg /corpora/registry/ee_deputies

cat data/downloaded/* | $(ca_getpipeline et) | 7zpipe /corpora/vert/parliament/ee_deputies.vert.7z
compilecorp ee_deputies --recompile-corpus
../deploy_to_beta.sh ee_deputies