# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("p"):
            if "table" in [x.name for x in element.parents]:
                continue
            elif element.get("style") is not None and element.get("style") == "text-align:justify":
                element_str = str(element)
                speaker = re.sub(r'<[^>]+>', "", element_str)
                speaker_pos = re.match(r'^[^\.]+\.', speaker)
                element_str = element.text
                if speaker_pos is not None:
                    speaker = speaker[:speaker_pos.end()]
                    speaker_parts = speaker.split(" ")
                    if speaker_parts[0].isupper() and ((len(speaker_parts) == 1) or (len(speaker_parts) > 1 and (speaker_parts[1].isupper() or "(" in speaker_parts[1]))):
                        if self.last_speaker is not None:
                            self.file.write("</speaker>\n")
                        self.last_speaker = re.sub(r'</?a[^>]*>', "", speaker)
                        self.last_speaker = self.last_speaker.strip()
                        self.file.write('<speaker name="%s">\n' % (self.last_speaker))
                        element_str = element.text[len(self.last_speaker):]
                element_str = element_str.strip()
                if len(element_str) > 0:
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
    

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
