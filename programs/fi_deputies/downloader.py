# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime
import openpyxl

API_KEY = None

remaining_urls = []

def json_recursive_discovery(json_struct: Any) -> None:
    try:
        try:
            if re.search(r'/Kansanedustajien.*puheenvuorot.*[0-9]{4}-[0-9]{4}\.xlsx', json_struct["uri"]) is not None:
                remaining_urls.append(json_struct["uri"])
                return
        except:
            pass
        for key in json_struct:
            json_recursive_discovery(json_struct[key])
    except:
        pass
    try:
        for item in json_struct:
            json_recursive_discovery(item)
    except:
        pass

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://cdn.contentful.com/spaces"):
        response = requests.get(url, verify=False, timeout=1200)
        response_json = response.json()
        json_recursive_discovery(response_json)
    elif re.search(r'/Kansanedustajien.*puheenvuorot.*[0-9]{4}-[0-9]{4}\.xlsx', url) is not None:
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, verify=False, timeout=1200)
        url_access_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")
        filename = re.sub(r'.*([0-9]{4})-([0-9]{4})\.xlsx$', "\\1_\\2.prevert", url)
        wb = openpyxl.open(filename=io.BytesIO(response.content))
        ws = wb["Puheenvuorot"]
        prevert_builder = PrevertBuilder(crawler, filename)
        for row in ws.iter_rows(max_col=13, min_row=2):
            data = {
                "context": row[0].value,
                "context_id": row[1].value,
                "sequence": row[2].value,
                "speech_id": row[3].value,
                "speaker_role": row[4].value,
                "speaker_first_name": row[5].value,
                "speaker_last_name": row[6].value,
                "parliamentary_group": row[7].value,
                "speech_type": row[8].value,
                "speech_start_time": row[9].value,
                "speech_end_time": row[10].value,
                "speech_url": row[12].value,
                "source_url": url, "url_access_time": url_access_time, "filename": filename
            }
            try:
                data["date"] = data["speech_start_time"].strftime("%Y-%m-%d")
                data["date_year"], data["date_month"], data["date_day"] = data["date"].split("-")
                data["date_year"] = str(int(data["date_year"]))
                data["date_month"] = str(int(data["date_month"]))
                data["date_day"] = str(int(data["date_day"]))
            except AttributeError:
                data["date"] = "===NONE==="
                data["date_year"] = "===NONE==="
                data["date_month"] = "===NONE==="
                data["date_day"] = "===NONE==="
            prevert_builder.write(row[11].value, data)
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    global API_KEY
    config = load_config(args.config)
    if API_KEY is None:
        API_KEY = config.get("api_key", None)
    if API_KEY is not None:
        remaining_urls.append("https://cdn.contentful.com/spaces/ihup72fnxs9n/environments/master/entries?access_token=%s&locale=fi-FI&content_type=datasetPageRichTextContent&order=sys.updatedAt" % (API_KEY, ))
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        last_id = crawler.dbcursor.execute("SELECT id FROM downloaded ORDER BY id DESC LIMIT 1").fetchall()
        if len(last_id) > 0:
            crawler.dbcursor.execute("DELETE FROM downloaded WHERE id = ?", (last_id[0][0], ))
            crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

