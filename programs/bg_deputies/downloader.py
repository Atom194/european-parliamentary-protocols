# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import re
import datetime
import json

re_zip_filename = re.compile("s[0-9]{6}.htm")

remaining_urls = []
year_now = int(datetime.datetime.utcnow().strftime("%Y"))

for year in range(year_now-1, year_now + 1):
    for month in range(1, 13):
        remaining_urls.append("https://www.parliament.bg/api/v1/archive-period/bg/Pl_StenV/%s/%s/0/0" % (year, month))

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    response = requests.get(url, verify=False, timeout=1200)
    if url.startswith("https://www.parliament.bg/api/v1/archive-period/bg/Pl_StenV/"):
        documents = json.loads(response.text)
        for document in documents:
            remaining_urls.append("https://www.parliament.bg/api/v1/pl-sten/%s" % (document["t_id"], ))
    elif url.startswith("https://www.parliament.bg/api/v1/pl-sten/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        filename = url.split("/")[-1] + ".prevert"
        response_json = response.json()
        year, month, day = re.sub(r'(....)-(..)-(..)', "\\1 \\2 \\3", response_json["Pl_Sten_date"]).split()
        day = int(day)
        month = int(month)
        year = int(year)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": response_json["Pl_Sten_date"], "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.BytesIO(response_json["Pl_Sten_body"].encode("utf-8")))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

