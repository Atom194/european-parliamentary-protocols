# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

import smtplib
from email.mime.text import MIMEText

from typing import List

class Mail:
    def __init__(self, smtp_servers: List[str], recepients: List[str], sender: str, timeout: float=60) -> None:
        self.recepients = recepients
        self.sent = False
        self.sender = sender
        self.timeout = timeout
        self.smtp_servers = smtp_servers
        self._smtp_server = None
        self.connect_to_server()
        
    
    def connect_to_server(self) -> None:
        for smtp_server in self.smtp_servers:
            try:
                self._smtp_server = smtplib.SMTP(smtp_server, timeout=self.timeout)
                return
            except:
                pass
        raise Exception("Unable to connect to any SMTP server.")
    
    def notify(self, subject: str, message: str, check_sent: bool = True) -> None:
        if check_sent:
            if self.sent:
                return
            self.sent = True
        mes = MIMEText(message)
        mes['Subject'] = subject
        mes['To'] = ",".join(self.recepients)
        try:
            self._smtp_server.sendmail(self.sender, self.recepients, mes.as_string())
        except smtplib.SMTPServerDisconnected:
            self.connect_to_server()
            self._smtp_server.sendmail(self.sender, self.recepients, mes.as_string())

    def close(self) -> None:
        self._smtp_server.quit()
        
        