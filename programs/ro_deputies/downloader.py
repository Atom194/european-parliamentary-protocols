# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

month_table = {
    "ianuarie": 1,
    "februarie": 2,
    "martie": 3,
    "aprilie": 4,
    "mai": 5,
    "iunie": 6,
    "iulie": 7,
    "august": 8,
    "septembrie": 9,
    "octombrie": 10,
    "noiembrie": 11,
    "decembrie": 12,
}

def int_to_roman(number: int) -> str:
    lab = {}
    lab[1000] = "M"
    lab[900] = "CM"
    lab[500] = "D"
    lab[400] = "CD"
    lab[100] = "C"
    lab[90] = "XC"
    lab[50] = "L"
    lab[40] = "XL"
    lab[10] = "X"
    lab[9] = "IX"
    lab[5] = "V"
    lab[4] = "IV"
    lab[1] = "I"
    roman_number = ""
    while number > 0:
        for key in lab:
            roman_number += lab[key] * (number // key)
            number = number % key
    return roman_number

remaining_urls = [{"url": "https://www.cdep.ro/pls/steno/steno2015.cauta",
                   "data": "idl=1&cam=&nrp=&anp=&vor=&vor=&vor=&sir=&sep=and&ts=0&dat1=1&dat1=1&dat1=%s&dat2=31&dat2=12&dat2=%s&ord=0&pag=200000" % (str(int(datetime.datetime.utcnow().strftime("%Y")) - 2), str(int(datetime.datetime.utcnow().strftime("%Y")) - 2)),
                   "type": "search",
                   }]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url["url"], ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url["type"] == "search":
        response = requests.post(url["url"], data=url["data"], verify=False)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        was_steno_found = False
        for element in elements:
            if element.get("href") is not None and element.get("href").startswith("/pls/steno/steno2015.stenograma"):
                was_steno_found = True
                doc_number = int(re.sub(r'/pls/steno/steno2015.stenograma.*[\?&]ids=([0-9]+).*', "\\1", element.get("href")))
                remaining_urls.append({
                "url": "https://www.cdep.ro/pls/steno/steno2015.stenograma?ids=%s" % (str(doc_number), ),
                "type": "steno"
            })
        if was_steno_found:
            year = int(re.sub(r'idl=1&cam=&nrp=&anp=&vor=&vor=&vor=&sir=&sep=and&ts=0&dat1=1&dat1=1&dat1=([0-9]+)&dat2=31&dat2=12&dat2=[0-9]+&ord=0&pag=200000', "\\1", url["data"]))
            remaining_urls.append({
                    "url": "https://www.cdep.ro/pls/steno/steno2015.cauta",
                    "data": "idl=1&cam=&nrp=&anp=&vor=&vor=&vor=&sir=&sep=and&ts=0&dat1=1&dat1=1&dat1=%s&dat2=31&dat2=12&dat2=%s&ord=0&pag=200000" % (str(year + 1), str(year + 1)),
                    "type": "search",
                    })
    elif url["type"] == "steno":
        crawler.log("Downloading: %s" % (url["url"], ), LOG_TYPE.MESSAGE)
        try:
            response = requests.get(url["url"], verify=False, timeout=1200)
        except requests.exceptions.SSLError as e:
            if "bad signature" in str(e):
                crawler.log("Invalid signature for url %s Exceprion: %s" % (url, str(e)), LOG_TYPE.WARNING)
                return None
            raise e
        filename = url["url"][55:] + ".prevert"

        date_match = re.search(r'din\s+[0-9]{1,2}\s+[a-zńź]+\s+[0-9]{4}', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'din\s+([0-9]{1,2})\s+([a-zńź]+)\s+([0-9]{4})', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url["url"], "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url["url"], filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

