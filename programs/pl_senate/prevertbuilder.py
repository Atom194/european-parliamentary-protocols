# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
            elif element.name == "head":
                continue
            elif element.name == "a":
                continue
            elif element.name == "h1":
                continue
            elif element.name == "h2":
                continue
            elif element.name == "h3":
                if self.last_speaker is not None:
                    self.file.write("</speaker>\n")
                self.last_speaker = element.text
                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
            elif element.name == "p":
                if element.get("class") is not None and element.get("class") == ["bodytext-P"]:
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                elif element.get("class") is not None and element.get("class") == ["wckom-P"]:
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                elif element.get("class") is not None and element.get("class") == ["pos-P"]:
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                elif element.get("class") is not None and element.get("class") == ["oskierow-P"]:
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                elif element.get("class") is not None and element.get("class") == ["haslo-P"]:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = element.text
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                elif element.get("class") is not None and element.get("class") == ["centr-P"]:
                    self.file.write('<note type="other">\n' + element.text + "\n</note>\n")
                else:
                    unchatched_element(element)
            elif element.name == "script":
                continue
            elif element.name == "div":
                if element.get("class") is not None and element.get("class") == ["stenogram-przypisy"]:
                    continue # from URL: https://www.senat.gov.pl/prace/posiedzenia/przebieg,480,3.html
                else:
                    unchatched_element(element)
            else:
                unchatched_element(element)

    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all('div', {'id': 'jq-stenogram-tresc'}):
            self._write_rec(element)
    

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
