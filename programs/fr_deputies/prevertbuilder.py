# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<a href="#">[^<]*</a>[:][ ]*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read().decode("utf-8"), "html.parser")
        elements = page.find_all('section', {'class': 'section crs-cr-content'})
        for element in elements:
            for element2 in element.find_all():
                if element2.name == "p" and element2.get("class") == ["orateur"]:
                    element2_str = str(element2)
                    element2_str = re.sub(r'<[^>]+>', "", element2_str)
                    if self.last_speaker is not None:
                        self.file.write('</speaker>\n')
                    self.last_speaker = element2_str
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                if element2.name == "span" and element2.get("class") == ["intervention"]:
                    speech = element2.text.strip()
                    speech = re.sub(r' +', " ", speech)
                    self.file.write("<p>\n" + speech + "\n</p>\n")
    

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)