#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp it_senate.reg /corpora/registry/it_senate

cat data/downloaded/* | $(ca_getpipeline it) | 7zpipe /corpora/vert/parliament/it_senate.vert.7z
compilecorp it_senate --recompile-corpus
../deploy_to_beta.sh it_senate