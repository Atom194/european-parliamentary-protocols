# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import datetime
import re

current_year = int(datetime.datetime.utcnow().strftime("%Y"))

remaining_urls = ["https://www.oireachtas.ie/en/debates/find/?page=1&datePeriod=dates&debateType=all&toDate=31%2F12%2F" + str(current_year - 1) + "&fromDate=01%2F01%2F" + str(current_year - 1) + "&resultsPerPage=100"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.oireachtas.ie/en/debates/find/?"):
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        found_steno = False
        for element in elements:
            if element.get("href") is not None and element.get("href").startswith("/en/debates/debate/"):
                found_steno = True
                remaining_urls.append("https://www.oireachtas.ie" + element.get("href"))
        if found_steno:
            page_number, year = re.sub(r'https://www\.oireachtas\.ie/en/debates/find/\?page\=([0-9]+)&datePeriod\=dates&debateType\=all&toDate\=31%2F12%2F([0-9]{4})&fromDate\=01%2F01%2F[0-9]{4}&resultsPerPage\=100', "\\1 \\2", url).split()
            page_number = int(page_number)
            year = int(year)
            remaining_urls.append("https://www.oireachtas.ie/en/debates/find/?page=" + str(page_number + 1) + "&datePeriod=dates&debateType=all&toDate=31%2F12%2F" + str(year) + "&fromDate=01%2F01%2F" + str(year) + "&resultsPerPage=100")
            if page_number == 1:
                remaining_urls.append("https://www.oireachtas.ie/en/debates/find/?page=1&datePeriod=dates&debateType=all&toDate=31%2F12%2F" + str(year + 1) + "&fromDate=01%2F01%2F" + str(year + 1) + "&resultsPerPage=100")
    elif url.startswith("https://www.oireachtas.ie/en/debates/debate/") and url.count("/") == 8:
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        filename = "_".join(url.split("/")[-3:-1]) + ".prevert"
        date = re.sub(r'.*/(....-..-..)/.*', "\\1", url)
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        for element in elements:
            if element.get("href") is not None and element.get("href").startswith("/en/debates/debate/") and element.get("href").count("/") == 7:
                stenoprotocol = requests.get("https://www.oireachtas.ie" + element.get("href"), timeout=1200)
                prevert_builder.write(io.BytesIO(stenoprotocol.content))
        prevert_builder.build()
        return filename
        
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

