# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup
from html5lib.html5parser import parse
from html5lib.serializer import serialize

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def _write_rec(self, elements):
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)
        
        for element in elements.find_all(recursive=False):
            if element.name == "td":
                self._write_rec(element)
            elif element.name == "tr":
                self._write_rec(element)
            elif element.name == "a":
                self._write_rec(element)
            elif element.name == "table":
                self._write_rec(element)
            elif element.name == "tbody":
                self._write_rec(element)
            elif element.name == "ol":
                self._write_rec(element)
            elif element.name == "ul":
                self._write_rec(element)
            elif element.name == "i":
                self.file.write('<note type="other">\n')
                self.file.write('<p>\n' + element.text.strip() + '\n</p>\n')
                self.file.write('</note>\n')
            elif element.name == "p":
                element_str = str(element)
                if '<b>' in element_str:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = element.text
                    self.last_speaker = self.last_speaker.strip()
                    if len(self.last_speaker) == 0:
                        self.last_speaker = None
                        continue
                    if self.last_speaker[-1] == ":":
                        self.last_speaker = self.last_speaker[:-1]
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                elif '<i>' in element_str:
                    self.file.write('<note type="other">\n')
                    self.file.write('<p>\n' + element.text.strip() + '\n</p>\n')
                    self.file.write('</note>\n')
                else:
                    self.file.write('<p>\n' + element.text.strip() + '\n</p>\n')
            elif element.name == "li":
                self.file.write('<p>\n' + element.text.strip() + '\n</p>\n')
            elif element.name == "img":
                continue
            else:
                unchatched_element(element)

    def write(self, html_file: IO) -> None:
        # fix missing end tags
        token_stream = parse(html_file.read())
        fixed_file = serialize(token_stream, omit_optional_tags=False)

        page = BeautifulSoup(fixed_file, "html.parser")
        for element in page.find_all("table"):
            self._write_rec(element)


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
