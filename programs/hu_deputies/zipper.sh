#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


find data/downloaded/ | grep "^data/downloaded/" > tmp_files.txt

cat tmp_files.txt | zip -rv -j -u data/downloaded/prevert.zip -@ && for f in $(cat tmp_files.txt) ; do rm "$f" ; done
rm tmp_files.txt