# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<a href="#">[^<]*</a>[:][ ]*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        def resolve_speach(element):
            element_str = str(element)
            speaker_end_pos = element_str.find("</h4>")

            speech = element_str[speaker_end_pos:]
            speech = re.sub(r'<[^>]*>|\n', "", speech)
            speech = re.sub(r'[ \t]+', " ", speech)
            speech = speech.strip()
            if len(speech) == 0:
                return

            speaker_name = element_str[:speaker_end_pos]
            speaker_name = re.sub(r'<[^>]*>|\n', "", speaker_name)
            speaker_name = re.sub(r'[ \t]+', " ", speaker_name)
            speaker_name = speaker_name.strip()
            speaker_name = re.sub(r'^.?.:.. (.*)', "\\1", speaker_name)

            if len(speaker_name) != 0:
                if self.last_speaker is not None:
                    self.file.write("</speaker>\n")
                self.last_speaker = speaker_name
                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
            self.file.write("<p>\n" + speech + "\n</p>\n")


        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
            elif element.name == "head":
                continue
            elif element.name == "meta":
                continue
            elif element.name == "script":
                continue
            elif element.name == "main":
                self._write_rec(element)
            elif element.name == "div":
                if [x.name for x in element.parents] == ["main", "body", "html", "[document]"]:
                    self._write_rec(element)
                elif [x.name for x in element.parents] == ["div", "main", "body", "html", "[document]"]:
                    self._write_rec(element)
                elif [x.name for x in element.parents] == ["div", "div", "main", "body", "html", "[document]"]:
                    self._write_rec(element)
                elif [x.name for x in element.parents] == ["article", "div", "div", "div", "main", "body", "html", "[document]"] and element.get("class") == ['pb-4', 'speech-area']:
                    resolve_speach(element)
                else:
                    unchatched_element(element)
            elif element.name == "article":
                if [x.name for x in element.parents] == ["div", "div", "div", "main", "body", "html", "[document]"]:
                    self._write_rec(element)
                else:
                    unchatched_element(element)
            else:
                unchatched_element(element)

    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read().decode("utf-8"), "html.parser")
        self._write_rec(page)
    
    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)