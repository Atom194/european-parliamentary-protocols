#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp nl_deputies.reg /corpora/registry/nl_deputies

cat data/downloaded/* | $(ca_getpipeline nl) | 7zpipe /corpora/vert/parliament/nl_deputies.vert.7z
compilecorp nl_deputies --recompile-corpus
../deploy_to_beta.sh nl_deputies