# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime

re_zip_filename = re.compile("s[0-9]{6}.htm")

remaining_urls = ["https://www.eerstekamer.nl/archief_plenaire_vergaderingen?start_002=0", "https://www.eerstekamer.nl/archief_plenaire_vergaderingen?start_002=50", "https://www.eerstekamer.nl/archief_plenaire_vergaderingen?start_002=100", "https://www.eerstekamer.nl/archief_plenaire_vergaderingen?start_002=150"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.eerstekamer.nl/archief_plenaire_vergaderingen?"):
        response = requests.get(url, verify=False, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        for element in elements:
            if element.get("href") is not None and element.get("href").startswith("/plenaire_vergadering/"):
                date = re.sub(r'/plenaire_vergadering/([0-9]+)_?.*', "\\1", element.get("href"))
                remaining_urls.append("https://www.eerstekamer.nl/verslag/%s/verslag" % (date, ))
    elif url.startswith("https://www.eerstekamer.nl/verslag/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, verify=False, timeout=1200)
        date = re.sub(r'https://www.eerstekamer.nl/verslag/([0-9]+)/verslag', "\\1", url)
        filename = date + ".prevert"
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date[0:4] + "-" + date[4:6] + "-" + date[6:8], "date_year": date[0:4], "date_month": str(int(date[4:6])), "date_day": str(int(date[6:8]))})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

