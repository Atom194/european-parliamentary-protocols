# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, IO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser") # .decode("iso-8859-1")
        elements = page.find_all('div', {'class': 'cols'})
        for element in elements:
            for element2 in element.find_all():
                if element2.name == "div":
                    if element2.get("class") is not None and element2.get("class") == ["intervenant"]:
                        element2_str = str(element2)
                        element2_str = re.sub(r'<!--[^>]*-->', "", element2_str)
                        speaker = re.findall(re_speaker, element2_str)
                        if len(speaker) > 0:
                            if self.last_speaker is not None:
                                self.file.write("</speaker>\n")
                            self.last_speaker = "".join(speaker)
                            self.last_speaker = self.last_speaker.replace("\n", " ")
                            self.last_speaker = re.sub(r'<[^>]+>', "", self.last_speaker)
                            self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                            element2_str = re.sub(re_speaker, "", element2_str)
                        element2_str = re.sub(r'</?a[^>]*>', "", element2_str)
                        element2_str = re.sub(r'</?span[^>]*>', "", element2_str)
                        element2_str = re.sub(r'</?p[^>]*>', "", element2_str)
                        element2_str = re.sub(r'</?div[^>]*>', "", element2_str)
                        element2_str = element2_str.replace("\n", " ")
                        element2_str = re.sub(r' +', " ", element2_str)
                        element2_str = element2_str.strip()
                        self.file.write("<p>\n" + element2_str + "\n</p>\n")



    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)