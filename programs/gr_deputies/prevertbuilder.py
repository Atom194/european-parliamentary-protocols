# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
import docx
import re

re_speaker = re.compile(r'^[^:]*\([^:]+\):')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.is_final_document = True
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def write(self, docx_file: BinaryIO) -> None:
        document = docx.Document(docx_file)
        for paragraph in document.paragraphs:
            paragraph_text = paragraph.text
            speaker_match = re.search(re_speaker, paragraph_text)
            if speaker_match is not None:
                if self.last_speaker is not None:
                    self.file.write("</speaker>\n")
                self.last_speaker = paragraph_text[speaker_match.start():speaker_match.end()-1]
                self.last_speaker = self.last_speaker.strip()
                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                paragraph_text = paragraph_text[speaker_match.end():]
                    
            paragraph_text = paragraph_text.strip()
            if len(paragraph_text) > 0:
                if paragraph_text[0] == "(" and paragraph_text[-1] == ")":
                    self.file.write('<note type="other">\n')
                    self.file.write(paragraph_text)
                    self.file.write("\n</note>\n")
                else:
                    self.file.write("<p>\n")
                    self.file.write(paragraph_text)
                    self.file.write("\n</p>\n")
    
    def build(self):
        if not self.is_final_document:
            self.file.close()
            import os
            os.remove(self.crawler.dir_unconfirmed + "/" + self.filename)
            raise AssertionError("This document is not corected. Try again later.")
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)