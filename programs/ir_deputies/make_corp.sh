#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp ir_deputies.reg /corpora/registry/ir_deputies

cat data/downloaded/* | $(ca_getpipeline en) | 7zpipe /corpora/vert/parliament/ir_deputies.vert.7z
compilecorp ir_deputies --recompile-corpus
../deploy_to_beta.sh ir_deputies