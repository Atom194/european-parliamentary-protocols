#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp at_senate.reg /corpora/registry/at_senate

cat data/downloaded/* | $(ca_getpipeline de) | 7zpipe /corpora/vert/parliament/at_senate.vert.7z
compilecorp at_senate --recompile-corpus
../deploy_to_beta.sh at_senate