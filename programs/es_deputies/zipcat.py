#!/usr/bin/python3
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


import zipfile
import sys

for line in sys.stdin:
    line = line[:-1] if line[-1] == "\n" else line
    if line.endswith(".prevert"):
        with open(line, "r") as source:
            print(source.read())
    if line.endswith(".zip"):
        with open(line, "rb") as source:
            zip_file = zipfile.ZipFile(source)
            for file in zip_file.infolist():
                with zip_file.open(file) as prevert:
                    print(prevert.read().decode())