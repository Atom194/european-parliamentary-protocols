# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def write(self, xml_file: IO) -> None:
        def unchatched_element(element: str):
            self.crawler.log("In file %s: Uncatched: %s" % (self.filename, element), LOG_TYPE.WARNING)

        xml_root = ET.parse(xml_file).getroot()
        for element in xml_root:
            if element.tag == "Sumar":
                for element2 in element:
                    if element2.tag == "Text":
                        if element2.attrib.get("Senator", None) is None:
                            if element2.text is None:
                                continue
                            element_str = element2.text
                            element_str = re.sub(r'^<!\[CDATA\[', "", element_str)
                            element_str = re.sub(r'\]\]>$', "", element_str)
                            element_str = re.sub(r'<[^>]+>', "", element_str)
                            element_str = element_str.strip()
                            if len(element_str) > 0:
                                self.file.write("<p>\n")
                                self.file.write(element_str)
                                self.file.write("\n</p>\n")
                        else:
                            for element3 in element2:
                                if element3.tag == "Text":
                                    if element3.text is None:
                                        continue
                                    element_str = element3.text
                                    element_str = re.sub(r'^<!\[CDATA\[', "", element_str)
                                    element_str = re.sub(r']]>$', "", element_str)
                                    element_str = re.sub(r'<\[^>\]+>', "", element_str)
                                    element_str = element_str.strip()
                                    if len(element_str) > 0:
                                        self.file.write('<speaker name="%s">\n' % (element2.attrib["Senator"], ))
                                        self.file.write("<p>\n")
                                        self.file.write(element_str)
                                        self.file.write("\n</p>\n")
                                        self.file.write("</speaker>\n")
                                elif element3.tag == "Eticheta":
                                    continue
                                else:
                                    unchatched_element("%s -> %s -> %s" % (element.tag, element2.tag, element3.tag))
                    else:
                        unchatched_element("%s -> %s" % (element.tag, element2.tag))
            else:
                unchatched_element(element.tag)


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
