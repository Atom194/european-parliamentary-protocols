#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp pt_deputies.reg /corpora/registry/pt_deputies

cat data/downloaded/* | $(ca_getpipeline pt) | 7zpipe /corpora/vert/parliament/pt_deputies.vert.7z
compilecorp pt_deputies --recompile-corpus
../deploy_to_beta.sh pt_deputies