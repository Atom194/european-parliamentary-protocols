#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp it_deputies.reg /corpora/registry/it_deputies

cat data/downloaded/* | $(ca_getpipeline it) | 7zpipe /corpora/vert/parliament/it_deputies.vert.7z
compilecorp it_deputies --recompile-corpus
../deploy_to_beta.sh it_deputies