# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, IO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<span [^>]*style=[^>]*>.*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def uncatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
                else:
                    uncatched_element(element)
            elif element.name == "head":
                continue
            elif element.name == "div":
                if "class" in element.attrs and (element.attrs["class"] == ["WordSection1"] or element.attrs["class"] == ["Section2"] or element.attrs["class"] == ["WordSection2"] or element.attrs["class"] == ["Section5"]):
                    self._write_rec(element)
                else:
                    uncatched_element(element)
            elif element.name == "p":
                self._write_rec(element)
            elif element.name == "span":
                if element.get("lang") is not None and \
                    (
                     element.attrs["lang"] == "FR" or \
                     element.attrs["lang"] == "NL-BE" or \
                     element.attrs["lang"] == "NL" or \
                     element.attrs["lang"] == "IT" or \
                     element.attrs["lang"] == "FR-BE"
                    ):
                    element_str = str(element)
                    element_str = re.sub(r'<!--[^(-->)]*-->', "", element_str)
                    element_str = re.sub(r'<\?if !supportEmptyParas\?> <\?endif\?>', "", element_str)
                    element_str = re.sub(r'<o:p>', "", element_str)
                    element_str = re.sub(r'</o:p>', "", element_str)
                    element_str = re.sub("<span[^>]*>|</span[^>]*>", "", element_str)
                    element_str = re.sub("<br[^>]*>", "", element_str)
                    element_str = re.sub("<i[^>]*>", "", element_str)
                    element_str = re.sub("<b[^>]*>", "", element_str)
                    element_str = re.sub(r' +', " ", element_str)
                    element_str = element_str.strip()
                    if len(element_str) > 0:
                        if self.last_speaker is not None:
                            self.file.write('<speaker name="%s">\n<p lang="%s">\n' % (self.last_speaker, element.get("lang"), ) + element_str + "\n</p>\n</speaker>\n")
                        else:
                            self.file.write('<p lang="%s">\n' % (element.get("lang"), ) + element_str + "\n</p>\n")
                elif element.get("class") == ["oraspr"]:
                    element_str = str(element)
                    element_str = re.sub(r'<[^>]*>', "", element_str)
                    element_str = re.sub(r'\s+', " ", element_str)
                    element_str = element_str.strip()
                    self.last_speaker = element_str
                elif element.get("style") is not None and element.get("style").startswith("mso-bookmark:_Toc"):
                    continue
                else:
                    uncatched_element(element)
            elif element.name == "o:p":
                continue
            elif element.name == "i":
                if "span" not in [x.name for x in element.parents]:
                    continue
                else:
                    uncatched_element(element)
            elif element.name == "h2":
                continue
            else:
                uncatched_element(element)

    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        self._write_rec(page)
    
    def build(self):
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)