#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp fi_deputies.reg /corpora/registry/fi_deputies

cat data/downloaded/* | $(ca_getpipeline fi) | 7zpipe /corpora/vert/parliament/fi_deputies.vert.7z
compilecorp fi_deputies --recompile-corpus
../deploy_to_beta.sh fi_deputies