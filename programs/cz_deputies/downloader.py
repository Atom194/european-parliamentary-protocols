# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime

re_zip_filename = re.compile("s[0-9]{6}.htm")

month_table = {
    "leden": 1,
    "ledna": 1,
    "únor": 2,
    "února": 2,
    "březen": 3,
    "března": 3,
    "duben": 4,
    "dubna": 4,
    "květen": 5,
    "května": 5,
    "červen": 6,
    "června": 6,
    "červenec": 7,
    "července": 7,
    "srpen": 8,
    "srpna": 8,
    "září": 9,
    "říjen": 10,
    "října": 10,
    "listopad": 11,
    "listopadu": 11,
    "prosinec": 12,
    "prosince": 12,
}

def setup_urls() -> List[str]:
    sources = []
    year = int(datetime.datetime.now().strftime("%Y"))
    while year > 2000 and len(sources) < 2:
        response = requests.get("https://www.psp.cz/eknih/%sps/stenprot/zip/" % (str(year), ), verify=False, timeout=1200)
        if response.status_code == 200:
            sources.append("https://www.psp.cz/eknih/%sps/stenprot/zip/" % (str(year), ))
        year -= 1
    if len(sources) == 0:
        raise Exception("Unable to find data source.")
    return sources

remaining_urls = setup_urls()

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    response = requests.get(url, verify=False, timeout=1200)
    if url.endswith("/stenprot/zip/"):
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        for element in elements:
            if element.get("href").endswith("schuz.zip"):
                remaining_urls.append(url + element.get("href"))
    elif url.endswith("schuz.zip"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        year = int(re.sub(r'https://www.psp.cz/eknih/([0-9]+)ps/stenprot/zip/.+', "\\1", url))
        filename = str(year) + "_" + url.split("/")[-1][:-3] + "prevert"
        zip_file = zipfile.ZipFile(io.BytesIO(response.content))
        with zip_file.open("index.htm") as source:
            text = source.read().decode("windows-1250")
            text = text.replace("&nbsp;", " ")
            date_match = re.search(r'[0-9]{1,2}\. [a-z(&auml;)äûúřěčáří]+ [0-9]{4}', text)
            if date_match is None:
                day = "===NONE==="
                month = "===NONE==="
                year = "===NONE==="
                date = "===NONE==="
                crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
            else:
                date_text = text[date_match.start():date_match.end()]
                day, month, year = re.sub(r'([0-9]{1,2})\. ([a-z(&auml;)äûúřěčáří]+) ([0-9]{4})', "\\1 \\2 \\3", date_text).split()
                month = month.replace("&auml;", "ä")
                day = int(day)
                month = month_table[month]
                year = int(year)
                date = "%d-%02d-%02d" % (year, month, day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        for file in zip_file.infolist():
            if re_zip_filename.fullmatch(file.filename):
                with zip_file.open(file.filename) as source:
                    prevert_builder.write(source)
        try:
            prevert_builder.build()
        except AssertionError as e:
            if "not corected" in str(e):
                return None
            raise e
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

