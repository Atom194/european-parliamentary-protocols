# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<span class="stenovystupujici"><a href="[^"]*">[^<]*</a>  </span>[ ]*')
re_speaker_only = re.compile('<span class="stenovystupujici"><a href="[^"]*">([^<]*)</a>  </span>.*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
                else:
                    unchatched_element(element)
            elif element.name == "head":
                continue
            elif element.name == "div":
                self._write_rec(element)
            elif element.name == "p":
                if [x.name for x in element.parents] == ["body", "html", "[document]"] and \
                   element.attrs["class"] == ["stenonadpis"]:
                    self.file.write('<note type="title">\n' + element.text + "\n</note>\n")
                elif [x.name for x in element.parents] == ["div", "body", "html", "[document]"] and \
                    (
                        [x for x in element.parents][0].attrs["class"] == ["stenovystoupeni", "neautorizovano"] or \
                        [x for x in element.parents][0].attrs["class"] == ["stenovystoupeni"]
                    ):
                    element_str = str(element)
                    new_speaker_pos = re.search(re_speaker, element_str)
                    if new_speaker_pos is not None:
                        new_speaker = re.sub(re_speaker_only, "\\1", element_str)[3:-1]
                        element_str = element_str[:new_speaker_pos.start()] + element_str[new_speaker_pos.end():]
                        new_speaker = re.sub(r'\s+', " ", new_speaker)
                        new_speaker = re.sub(r'<[^>]+>', "", new_speaker)
                        new_speaker = new_speaker.strip()
                        if self.last_speaker is not None:
                            self.file.write("</speaker>\n")
                        self.file.write('<speaker name="%s">\n' % (new_speaker))
                        self.last_speaker = new_speaker
                    element_str = re.sub(r'\s+', " ", element_str)
                    element_str = re.sub(r'<[^>]+>', "", element_str)
                    element_str = element_str.strip()
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
                elif [x.name for x in element.parents] == ["body", "html", "[document]"] and \
                     (
                        element.attrs["class"] == ["stenotitul", "neautorizovano"] or \
                        element.attrs["class"] == ["stenotitul"]
                     ):
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                elif [x.name for x in element.parents] == ["body", "html", "[document]"] and \
                     (
                        element.attrs["class"] == ["stenopoznamka", "neautorizovano"] or \
                        element.attrs["class"] == ["stenopoznamka"]
                     ):
                    self.file.write('<note type="other">\n' + element.text + "\n</note>\n")
                elif [x.name for x in element.parents] == ["body", "html", "[document]"] and \
                     (
                        element.attrs["class"] == ["stenohlavicka", "neautorizovano"] or \
                        element.attrs["class"] == ["stenohlavicka"]
                     ):
                    self.file.write('<note type="header">\n' + element.text + "\n</note>\n")
                elif [x.name for x in element.parents] == ["body", "html", "[document]"] and \
                     (
                        element.attrs["class"] == ["stenotisk", "neautorizovano"] or \
                        element.attrs["class"] == ["stenotisk"]
                     ):
                    continue
                else:
                    unchatched_element(element)
            elif element.name == "a":
                if [x.name for x in element.parents] == ["body", "html", "[document]"]:
                    continue
                elif [x.name for x in element.parents] == ["div", "body", "html", "[document]"] and \
                   [x for x in element.parents][0].attrs["class"] == ["videoVystoupeni", "neautorizovano"]:
                    continue
                elif [x.name for x in element.parents] == ["div", "body", "html", "[document]"] and \
                   [x for x in element.parents][0].attrs["class"] == ["audioVystoupeni", "neautorizovano"]:
                    continue
                elif element.text == "Videozáznam k bodu schůze":
                    continue
                elif element.text == "Audiozáznam k bodu schůze":
                    continue
                elif element.text == "Audiozáznam celého jednání":
                    continue
                elif element.text == "Videozáznam celého jednání":
                    continue
                elif element.text == "Audiozáznam vystoupení":
                    continue
                elif element.text == "Videozáznam vystoupení":
                    continue
                else:
                    unchatched_element(element)
            else:
                unchatched_element(element)

    def write(self, html_file: BinaryIO) -> None:
        prevertical: List[str] = []
        page = BeautifulSoup(html_file.read().decode("windows-1250"), "html.parser")
        self._write_rec(page)
    
    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)