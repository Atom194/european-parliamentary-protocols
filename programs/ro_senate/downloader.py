# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re
import xml.etree.ElementTree as ET

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

month_table = {
    "ianuarie": 1,
    "februarie": 2,
    "martie": 3,
    "aprilie": 4,
    "mai": 5,
    "iunie": 6,
    "iulie": 7,
    "august": 8,
    "septembrie": 9,
    "octombrie": 10,
    "noiembrie": 11,
    "decembrie": 12,
}

def int_to_roman(number: int) -> str:
    lab = {}
    lab[1000] = "M"
    lab[900] = "CM"
    lab[500] = "D"
    lab[400] = "CD"
    lab[100] = "C"
    lab[90] = "XC"
    lab[50] = "L"
    lab[40] = "XL"
    lab[10] = "X"
    lab[9] = "IX"
    lab[5] = "V"
    lab[4] = "IV"
    lab[1] = "I"
    roman_number = ""
    while number > 0:
        for key in lab:
            roman_number += lab[key] * (number // key)
            number = number % key
    return roman_number

remaining_urls = [{"url": "https://www.senat.ro/exportdata.asmx/ListaStenogramelor?DeLaData=1753-01-01&LaData=9999-12-31",
                   "type": "search",
                   }]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url["url"], ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url["type"] == "search":
        response = requests.get(url["url"], verify=False, headers=HEADERS, timeout=1200)
        response_xml = ET.parse(io.StringIO(response.text)).getroot()
        for element in response_xml.iter():
            steno_id = element.attrib.get('StenogramaID', None)
            if steno_id is not None:
                remaining_urls.append({"url": "https://www.senat.ro/exportdata.asmx/Stenograma?UID=%s" % (steno_id, ), "id": steno_id, "type": "steno"})
    elif url["type"] == "steno":
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url["url"], verify=False, headers=HEADERS, timeout=1200)
        if response.status_code == 500:
            return "Server side error"
        filename = url["id"] + ".prevert"
        response_xml = ET.parse(io.StringIO(response.text)).getroot()
        date = response_xml.attrib["Data"]
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url["url"], "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url["url"], filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

