# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re
import zipfile

remaining_urls = ["https://data.riksdagen.se/data/anforanden/"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url == "https://data.riksdagen.se/data/anforanden/":
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and re.match(r"//data.riksdagen.se/dataset/anforande/anforande-[0-9]+.json.zip", element.get("href")) is not None:
                remaining_urls.insert(0, "https:" + element.get("href"))
    elif url.startswith("https://data.riksdagen.se/dataset/anforande/anforande-"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        filename = re.sub(r'https://data.riksdagen.se/dataset/anforande/anforande-([0-9]+).json.zip', "\\1", url) + ".prevert"
        zip_file = zipfile.ZipFile(io.BytesIO(response.content))
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename})
        for file in zip_file.infolist():
            with zip_file.open(file.filename) as source:
                prevert_builder.write(source, file.filename)
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
    crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
    crawler.dbcursor.commit()

    try:
        last_id = crawler.dbcursor.execute("SELECT id FROM downloaded ORDER BY id DESC LIMIT 1").fetchall()
        if len(last_id) > 0:
            crawler.dbcursor.execute("DELETE FROM downloaded WHERE id = ?", (last_id[0][0], ))
            crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

