#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp be_deputies.reg /corpora/registry/be_deputies

cat data/downloaded/* | $(ca_getpipeline fr) | 7zpipe /corpora/vert/parliament/be_deputies.vert.7z
compilecorp be_deputies --recompile-corpus
../deploy_to_beta.sh be_deputies