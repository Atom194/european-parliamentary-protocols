# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

remaining_urls = ["https://debates.parlamento.pt/catalogo/r3/dar/01/01/01"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.count("/") == 8:
        response = requests.get(url, timeout=1200)
        if response.status_code == 404:
            return None
        elif response.status_code != 200:
            raise Exception("Unexpected status code %s." % response.status_code)
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and re.match(r'/catalogo/r3/dar/[0-9]{2}/[0-9]{2}/[0-9]{2}/[0-9]{3}/[0-9\-]+', element.get("href")) is not None:
                remaining_urls.insert(0, "https://debates.parlamento.pt" + element.get("href"))
        _, _, _, _, _, _, _, legislative, session = url.split("/")
        legislative = int(legislative)
        session = int(session)
        remaining_urls.append("https://debates.parlamento.pt/catalogo/r3/dar/01/%02d/%02d" % (legislative, session + 1))
        if session == 1:
            remaining_urls.append("https://debates.parlamento.pt/catalogo/r3/dar/01/%02d/%02d" % (legislative + 1, session))
    if url.count("/") == 10:
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        _, _, _, _, _, _, _, legislative, session, number, date = url.split("/")
        response = requests.post('https://debates.parlamento.pt/pagina/export', data={"exportType": "txt", "exportControl": "documentoCompleto", "periodo": "r3", "publicacao": "dar", "serie": "01", "legis": legislative, "sessao": session, "numero": number, "data": date, "pagina": "1", "exportar": "Exportar"})
        legislative = int(legislative)
        session = int(session)
        number = int(number)
        filename = str(number) + "_" + date + ".prevert"
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "legislative": str(legislative), "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)
    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

