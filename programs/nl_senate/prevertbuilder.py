# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('^.*<strong>.*</strong>.*:')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in recurse_element.find_all(recursive=False):
            if element.name == "div":
                if element.get("class") is not None and element.get("class") == ["partext"]:
                    self._write_rec(element)
                elif element.get("id") is not None and element.get("id") == "main_content_wrapper":
                    self._write_rec(element)
                else:
                    unchatched_element(element)
            elif element.name == "p":
                element_str = str(element)
                element_str = re.sub(r'</?p[^>]*>', "", element_str)
                element_str = re.sub(r'\s+', " ", element_str)
                speaker_match = re.search(re_speaker, element_str)
                if speaker_match is not None:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    element_str = re.sub(r'<[^>]+>', "", element_str)
                    element_str = element_str.strip()
                    self.last_speaker = element_str[:-1]
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                    continue
                element_str = re.sub(r'<[^>]+>', "", element_str)
                element_str = element_str.strip()
                self.file.write("<p>\n")
                self.file.write(element_str)
                self.file.write("\n</p>\n")
            else:
                unchatched_element(element)

    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("main"):
            self._write_rec(element)
    
    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)