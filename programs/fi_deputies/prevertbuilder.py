# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<a href="#">[^<]*</a>[:][ ]*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str) -> None:
        self.is_final_document = True
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")


    def write(self, speach: str, metadata: Dict[str, str] = {}) -> None:
        if metadata["speaker_role"] == "-":
            metadata["speaker_role"] = "===NONE==="
        
        self.file.write("<doc")
        for key in metadata:
            self.file.write(' %s="%s"' % (key, metadata[key]))
        self.file.write(">\n")
        self.file.write('<speaker name="%s" role="%s">\n' % (metadata["speaker_first_name"] + " " + metadata["speaker_last_name"], metadata["speaker_role"]))
        self.file.write("<p>\n")
        speach = speach.strip()
        self.file.write("%s\n" % (speach, ))
        self.file.write("\n</p>\n")
        self.file.write("</speaker>\n")
        self.file.write("</doc>\n")

    
    def build(self):
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)