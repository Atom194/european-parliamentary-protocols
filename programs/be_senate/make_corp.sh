#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp be_senate.reg /corpora/registry/be_senate

cat data/downloaded/*.fr | $(ca_getpipeline fr) | 7zpipe /corpora/vert/parliament/be_senate.vert.7z
compilecorp be_senate --recompile-corpus
../deploy_to_beta.sh be_senate