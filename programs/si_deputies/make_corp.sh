#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp si_deputies.reg /corpora/registry/si_deputies

cat data/downloaded/* | $(ca_getpipeline sl) | 7zpipe /corpora/vert/parliament/si_deputies.vert.7z
compilecorp si_deputies --recompile-corpus
../deploy_to_beta.sh si_deputies