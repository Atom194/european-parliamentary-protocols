# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'^[^:]*[A-Z]{2,}[^:]*:')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: IO) -> None:
        lines = html_file.read().split("<br>")
        lines = lines[1:-1]
        for line in lines:
            line = re.sub(r'\s+', " ", line)
            line = re.sub(r'<br/>', "", line)
            line = line.strip()
            if len(line) > 0:
                line = re.sub(r'<[^>]*>', "", line)
                speaker_match = re.search(re_speaker, line)
                if speaker_match is not None:
                    new_speaker = line[speaker_match.start():speaker_match.end()-1]
                    # if new_speaker != "ORDEN DEL DIA" and re.search("[0-9]{4,}", new_speaker) is None and "?" not in new_speaker and re.search(r'\-\-', new_speaker) is None and "«" not in new_speaker and "»" not in new_speaker and ";" not in new_speaker and new_speaker.count(",") < 4 and ("El señor" == new_speaker[:8] or "La señora" == new_speaker[:9] or new_speaker.count(" ") < 10):
                    if "El señor" == new_speaker[:8] or "La señora" == new_speaker[:9]:
                        if self.last_speaker is not None:
                            self.file.write("</speaker>\n")
                        self.file.write('<speaker name="%s">\n' % (line[:speaker_match.end()-1], ))
                        line = line[speaker_match.end():]
                line = line.strip()
                if len(line) > 0:
                    line = "<p>\n" + line + "\n</p>\n"
                    self.file.write(line)
        return

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
