# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<a href="#">[^<]*</a>[:][ ]*')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
            elif element.name == "head":
                continue
            elif element.name == "meta":
                continue
            elif element.name == "p":
                if element.get("class") == ["Tekst"] or element.get("class") == ["TekstIndryk"]:
                    element_str = str(element)
                    element_str = re.sub(r'\s+', " ", element_str)
                    element_str = re.sub(r'<[^>]*>', "", element_str)
                    element_str = element_str.strip()
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
                elif element.get("class") == ["TalerTitel"]:
                    element_str = str(element)
                    role = re.sub('.*<span class="Bold">(.*)</span>.*', "\\1", element_str)
                    speaker_pos = re.search("\\(.*\\)", element_str)
                    if len(role) == 0 or speaker_pos is None:
                        unchatched_element(element)
                        continue
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    role = re.sub(r'\s+', " ", role)
                    role = re.sub(r'<[^>]+>', "", role)
                    role = role.strip()
                    if len(role) == 0:
                        role = "===NONE==="
                    self.last_speaker = {"role": role, "speaker": element_str[speaker_pos.start()+1:speaker_pos.end()-1]}
                    self.file.write('<speaker name="%s" role="%s">\n' % (self.last_speaker["speaker"], self.last_speaker["role"]))
                else:
                    unchatched_element(element)
            else:
                unchatched_element(element)

    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read().decode("utf-8"), "html.parser")
        self._write_rec(page)
    
    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)