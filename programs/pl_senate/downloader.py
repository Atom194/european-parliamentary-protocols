# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

month_table = {
    "styczeń": 1,
    "stycznia": 1,
    "luty": 2,
    "lutego": 2,
    "marzec": 3,
    "marca": 3,
    "kwiecień": 4,
    "kwietnia": 4,
    "maj": 5,
    "maja": 5,
    "czerwiec": 6,
    "czerwca": 6,
    "lipiec": 7,
    "lipca": 7,
    "sierpień": 8,
    "sierpnia": 8,
    "wrzesień": 9,
    "październik": 10,
    "października": 10,
    "paździenika": 10,
    "listopad": 11,
    "listopada": 11,
    "grudzień": 12,
    "grudnia": 12,
}

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

remaining_urls = ["https://www.senat.gov.pl/prace/posiedzenia/page,1.html?k=8&pp=100"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Remaining URLs: %s, Resolving URL: %s" % (len(remaining_urls), url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.senat.gov.pl/prace/posiedzenia/page,"):
        response = requests.get(url, headers=HEADERS, timeout=1200)
        page_number, legis = re.sub(r'https://www\.senat\.gov\.pl/prace/posiedzenia/page,([0-9]+)\.html\?k=([0-9]+)&pp=100', "\\1 \\2", url).split(" ")
        page_number = int(page_number)
        legis = int(legis)
        found_source = False
        page = BeautifulSoup(response.text, "html.parser")
        for element in page.find_all("a"):
            if element.get("href") is not None and element.get("href").startswith("/prace/posiedzenia/przebieg,"):
                found_source = True
                remaining_urls.append("https://www.senat.gov.pl" + element.get("href"))
        if found_source and page_number < 10:
            remaining_urls.append("https://www.senat.gov.pl/prace/posiedzenia/page,%s.html?k=%s&pp=100" % (page_number + 1, legis))
            if page_number == 1:
                remaining_urls.append("https://www.senat.gov.pl/prace/posiedzenia/page,%s.html?k=%s&pp=100" % (page_number, legis + 1))
    if re.match(r'https://www.senat.gov.pl/prace/posiedzenia/przebieg,[0-9]+,[0-9]+.html', url) is not None:
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, allow_redirects=True, headers=HEADERS, timeout=1200)
        filename = re.sub(r'https://www.senat.gov.pl/prace/posiedzenia/przebieg,([0-9]+),([0-9]+).html', "\\1_\\2.prevert", url).replace(",", "_")

        date_match = re.search(r'[0-9]{1,2}\s+[a-zńź]+\s+[0-9]{4}\s+r\.', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'([0-9]{1,2})\s+([a-zńź]+)\s+([0-9]{4})\s+r\.', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)
    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

