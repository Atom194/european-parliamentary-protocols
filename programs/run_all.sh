#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


WF=$(pwd)

while true
do
    for dirpath in $(ls -dp */)
    do
        cd ./$dirpath
        screen -d -S ${dirpath::-2}-$(date +"%Y%m%d") -m ./run.sh
        cd $WF
    done
    sleep 86400
done
