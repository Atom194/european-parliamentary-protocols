#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp cz_senate.reg /corpora/registry/cz_senate

cat data/downloaded/* | $(ca_getpipeline cs) | 7zpipe /corpora/vert/parliament/cz_senate.vert.7z
compilecorp cz_senate --recompile-corpus
../deploy_to_beta.sh cz_senate