# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
import datetime
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element

remaining_urls = ["https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?idNumero=1&idLegislatura=19"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        id, legis = re.sub(r'https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx\?idNumero=([0-9]+)&idLegislatura=([0-9]+)', "\\1 \\2", url).split(" ")
        id = int(id)
        legis = int(legis)
        remaining_urls.append("https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?idNumero=%s&idLegislatura=%s" % (id + 1, legis))
        if id == 1:
            remaining_urls.append("https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?idNumero=1&idLegislatura=%s" % (legis + 1, ))
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        if "The remote server returned an error: (404)" in response.text:
            return None
        id, legis = re.sub(r'https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx\?idNumero=([0-9]+)&idLegislatura=([0-9]+)', "\\1 \\2", url).split(" ")
        id = int(id)
        legis = int(legis)
        remaining_urls.append("https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?idNumero=%s&idLegislatura=%s" % (id + 1, legis))
        if id == 1:
            remaining_urls.append("https://documenti.camera.it/apps/resoconto/getXmlStenografico.aspx?idNumero=1&idLegislatura=%s" % (legis + 1, ))

        filename = "%s_%s.prevert" % (legis, id)
        stenoprotocol_xml = ET.parse(io.StringIO(response.text)).getroot()
        year = int(stenoprotocol_xml.attrib["anno"])
        month = int(stenoprotocol_xml.attrib["mese"])
        day = int(stenoprotocol_xml.attrib["giorno"])
        date = "%d-%02d-%02d" % (year, month, day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})   
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
        
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

