# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import datetime
import re

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

remaining_urls = ["https://www.ft.dk/da/dokumenter/dokumentlister/referater?pageSize=200&startDate=10000101&endDate=99991231&pageNumber=1"]

def is_downloaded(crawler: Crawler, url: str) -> bool:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    return len(cursor_url_match.fetchall()) != 0

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    if is_downloaded(crawler, url):
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.ft.dk/da/dokumenter/dokumentlister/referater?pageSize=200&startDate=10000101&endDate=99991231&pageNumber="):
        response = requests.get(url, headers=HEADERS, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        href_found = False
        for element in elements:
            print(element.get("href"))
            if element.get("href") is not None and element.get("href").startswith("/forhandlinger/") and not is_downloaded(crawler, "https://www.ft.dk" + element.get("href")):
                href_found = True
                if element.get("href").endswith(".htm") or element.get("href").endswith(".html"):
                    remaining_urls.insert(0, "https://www.ft.dk" + element.get("href"))
                elif element.get("href").endswith(".pdf"):
                    pass
                else:
                    crawler.log("Unknown url ending for: '%s'" % (element.get("href"), ), log_type=LOG_TYPE.WARNING)
        if href_found:
            page_number = int(url.split("=")[-1]) + 1
            remaining_urls.insert(0, "=".join(url.split("=")[:-1]) + "=%s" % (page_number, ))
    elif url.startswith("https://www.ft.dk/forhandlinger/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, headers=HEADERS, timeout=1200)
        filename = url.split("/")[-1][:-3] + "prevert"
        date = re.sub(r'.*(....-..-..).*', "\\1", filename)
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")
    #parser.add_argument("-d", "--dir", default="data", help="Directory where downloaded data is stored.")
    #parser.add_argument("-c", "--cleandata", help="Downloaded data are stored after applying get_metadata function.")
    #parser.add_argument("-s", "--cleandata", help="Periodicly checked addresses for new URLs to download from.")

    args = parser.parse_args()
    run(args)

