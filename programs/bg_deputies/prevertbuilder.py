# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<span style="mso-spacerun:yes"> </span>[^<]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def write(self, html_file: BinaryIO) -> None:
        for line in html_file.readlines():
            line = line.decode("utf-8")
            line = re.sub("[ ]*<br />$", "", line)
            line = line.strip()
            if len(line) == 0:
                continue

            name_pos = line.find(":")
            if name_pos != -1:
                new_speaker = line[:name_pos]
                note_pos = re.search("\(.*\)",new_speaker)
                if note_pos is not None:
                    self.file.write('<note type="other">\n')
                    self.file.write(new_speaker[note_pos.start():note_pos.end()])
                    self.file.write('\n</note>\n')
                    new_speaker = new_speaker[:note_pos.start()] + new_speaker[note_pos.end():]
                    new_speaker = new_speaker.strip()
                if new_speaker.isupper():
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = new_speaker
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                    line = re.sub("^:[ ]*", "", line[name_pos:])
            self.file.write("<p>\n" + line + "\n</p>\n")

    
    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)