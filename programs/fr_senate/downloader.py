# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import datetime
import re

# For all data:
#remaining_urls = ["http://www.senat.fr/seances/seances.html"]

remaining_urls = []
year = int(datetime.datetime.utcnow().strftime("%Y"))
for y in range(year - 1, year + 1):
    for m in range(1, 13):
        remaining_urls.append("https://www.senat.fr/seances/s%04d%02d/s%04d%02d.html" % (y, m, y, m))


def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url == "http://www.senat.fr/seances/seances.html":
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        for element in elements:
            if element.get("href") is not None and element.get("class") is not None and element.get("href").startswith("s") and element.get("class") == ["link"]:
                remaining_urls.append("http://www.senat.fr/seances/" + element.get("href"))
    elif re.match(r'http://www\.senat\.fr/seances/s[0-9]{6}/s[0-9]{6}\.html', url):
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        for element in elements:
            if element.get("href") is not None and re.match(r's[0-9]{8}/s[0-9]{8}_mono.html', element.get("href")):
                remaining_urls.insert(0, "/".join(url.split("/")[:-1]) + "/" + element.get("href"))
    elif url.endswith("_mono.html"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        try:
            page = response.content.decode("utf-8")
        except UnicodeDecodeError:
            try:
                page = response.content.decode("utf-16")
            except UnicodeDecodeError:
                page = response.content.decode("utf-8", errors="ignore")
        bs_page = BeautifulSoup(page, "html.parser")
        for element in bs_page.find_all("span"):
            if element.get("class") is not None and "text-bg-warning" in element.get("class") and "version provisoire" in element.text:
                return None
        filename = url.split("/")[-1][:-4] + "prevert"
        date = re.sub(r's(....)(..)(..)_mono.prevert', "\\1-\\2-\\3", filename)
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(page))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

