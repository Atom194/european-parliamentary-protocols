#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp de_deputies.reg /corpora/registry/de_deputies

cat data/downloaded/* | $(ca_getpipeline de) | 7zpipe /corpora/vert/parliament/de_deputies.vert.7z
compilecorp de_deputies --recompile-corpus
../deploy_to_beta.sh de_deputies