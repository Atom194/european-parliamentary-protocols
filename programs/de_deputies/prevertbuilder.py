# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'[^0-9\s]+\s[^0-9\s]+\s?\(.+\):\n|[^0-9\s]+\s[^0-9\s]+\s?:\n')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, json_text: IO) -> None:
        paragraph = []
        for line in json_text.readlines():
            if re.match(re_speaker, line) is not None:
                text = "".join(paragraph)
                text = re.sub(r'\s+', " ", text)
                text = text.strip()
                self.file.write("<p>\n" + text + "\n</p>\n")
                if self.last_speaker is not None:
                    self.file.write("</speaker>\n")
                line = line[:-2]
                line = re.sub(r'\s+', " ", line)
                line = line.strip()
                self.last_speaker = line
                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                paragraph = []
            else:
                paragraph.append(line)


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
