# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re

API_KEY: Optional[str] = None

remaining_urls = []

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (str(url), ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    full_url = 'https://search.dip.bundestag.de/api/v1/plenarprotokoll-text/' + str(url)
    crawler.log("Resolving URL: %s" % (full_url, ), LOG_TYPE.PRINT)
    response = requests.get(full_url + "?apikey=" + API_KEY, timeout=1200)
    if response.status_code == 200:
        response_json = response.json()
        if "code" in response_json:
            return None
        if "text" not in response_json:
            return None
        crawler.log("Downloading: %s" % (full_url, ), LOG_TYPE.MESSAGE)
        remaining_urls.append(url + 1)
        filename = str(url) + ".prevert"
        year, month, day = response_json["datum"].split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": full_url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": response_json["datum"], "dokument_nummer": response_json["dokumentnummer"], "title": response_json["titel"], "last_upadte": response_json["aktualisiert"], "pdf_url": response_json["fundstelle"]["pdf_url"], "date_year": str(year), "date_month": str(month), "date_day": str(day)})   
        prevert_builder.write(io.StringIO(response_json["text"]))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    global API_KEY

    config = load_config(args.config)
    if API_KEY is None:
        API_KEY = config["api_key"]
        old_cursor = "a"
        new_cursor = ""
        while old_cursor != new_cursor:
            url = 'https://search.dip.bundestag.de/api/v1/plenarprotokoll?f.datum.start=1000-01-11&f.datum.end=3000-01-15&format=json&apikey=' + API_KEY
            if len(new_cursor) > 0:
                url += "&cursor=" + new_cursor
            response_json = requests.get(url, timeout=1200).json()
            for document in response_json["documents"]:
                remaining_urls.append(int(document["id"]))
            old_cursor = new_cursor
            new_cursor = response_json["cursor"]
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), str(url), filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

