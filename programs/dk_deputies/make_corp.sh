#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp dk_deputies.reg /corpora/registry/dk_deputies

cat data/downloaded/* | $(ca_getpipeline da) | 7zpipe /corpora/vert/parliament/dk_deputies.vert.7z
compilecorp dk_deputies --recompile-corpus
../deploy_to_beta.sh dk_deputies