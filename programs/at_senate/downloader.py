# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re
import os

month_table = {
    "Jänner": 1,
    "Januar": 1,
    "Februar": 2,
    "März": 3,
    "April": 4,
    "Mai": 5,
    "Juni": 6,
    "Juli": 7,
    "August": 8,
    "September": 9,
    "Oktober": 10,
    "November": 11,
    "Dezember": 12,
}

remaining_urls = ["https://www.parlament.gv.at/gegenstand/BR/BRSITZ/893?selectedStage=111&json=true"]

def json_recursive_discovery(json_struct: Any) -> None:
    if isinstance(json_struct, str):
        if re.match(r'/dokument/[^/]+/BRSITZ/[0-9]+/fnameorig_[0-9]+.html.*', json_struct) is not None:
            remaining_urls.insert(0, "https://www.parlament.gv.at" + json_struct.split("#")[0])
        return
    try:
        for key in json_struct:
            json_recursive_discovery(json_struct[key])
    except:
        pass
    try:
        for item in json_struct:
            json_recursive_discovery(item)
    except:
        pass

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://www.parlament.gv.at/gegenstand/"):
        response = requests.get(url, timeout=1200)
        try:
            response_json = response.json()
        except requests.exceptions.JSONDecodeError:
            return None
        json_recursive_discovery(response_json)
        meeting_number = int(re.sub(r'https://www.parlament.gv.at/gegenstand/[^/]+/BRSITZ/([0-9]+)\?selectedStage=111&json=true', "\\1", url))
        remaining_urls.append("https://www.parlament.gv.at/gegenstand/BR/BRSITZ/%s?selectedStage=111&json=true" % (str(meeting_number + 1)))
    elif url.startswith("https://www.parlament.gv.at/dokument/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        date_match = re.search(r'[0-9]{1,2}\. [A-Z][a-z(&auml;)ä]+ [0-9]{4}', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'([0-9]{1,2})\. ([A-Z][a-z(&auml;)ä]+) ([0-9]{4})', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)
        filename = url.split("/")[-1][:-5] + ".prevert"
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        last_url = crawler.dbcursor.execute("SELECT url FROM downloaded ORDER BY id DESC LIMIT 1").fetchall()

        if len(last_url) != 0:
            global remaining_urls
            remaining_urls = []
            meeting_number = re.sub(r'https://www.parlament.gv.at/dokument/[^/]+/BRSITZ/([0-9]+)/fnameorig_.+.html', "\\1", last_url[0][0])
            meeting_number = int(meeting_number)
            remaining_urls.append("https://www.parlament.gv.at/gegenstand/BR/BRSITZ/%s?selectedStage=111&json=true" % (str(meeting_number + 1)))

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except Exception as e:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

