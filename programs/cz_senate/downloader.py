# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import datetime

re_zip_filename = re.compile("s[0-9]{6}.htm")

remaining_urls = ["https://www.senat.cz/xqw/xervlet/pssenat/finddoc?typdok=steno"]

def is_downloaded(crawler: Crawler, url: str) -> bool:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    return len(cursor_url_match.fetchall()) != 0

def process_url(crawler: Crawler,  url: str) -> None:
    if is_downloaded(crawler, url):
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.endswith('finddoc?typdok=steno'):
        with requests.Session() as session:
            response = session.get(url, verify=False, timeout=1200)
            page = BeautifulSoup(response.text, "html.parser")
            elements = page.find_all("a")
            for element in elements:
                if element.get("href").startswith("/xqw/webdav/pssenat/original/"):
                    remaining_urls.append("https://www.senat.cz" + element.get("href"))
            is_last_page = False
            while not is_last_page:
                crawler.log("Going on next page...", LOG_TYPE.PRINT)
                response = session.post('https://www.senat.cz/xqw/xervlet/pssenat/finddoc?typdok=steno', data={"cid": "pssenat_finddoc.pFindDoc.pageList", "navigator.gowhere": "", "navigator.button.next": ">"}, verify=False)
                page = BeautifulSoup(response.text, "html.parser")
                elements = page.find_all("a")
                found_new_urls = False
                for element in elements:
                    if not is_downloaded(crawler, "https://www.senat.cz" + element.get("href")) and element.get("href").startswith("/xqw/webdav/pssenat/original/"):
                        remaining_urls.insert(0, "https://www.senat.cz" + element.get("href"))
                        found_new_urls = True
                if not found_new_urls:
                    break

                # check if page is a last page
                is_last_page = True
                elements = page.find_all("input")
                for element in elements:
                    if element.get("value") == ">":
                        is_last_page = False
    elif url.startswith("https://www.senat.cz/xqw/webdav/pssenat/original/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, verify=False, timeout=1200)
        if response.headers['Content-Type'] == 'application/msword':
            crawler.log("%s ignored as MSWORD" % (url, ), LOG_TYPE.PRINT)
            return "Ignored: MSWORD"
        filename = "_".join(url.split("/")[-2:]) + ".prevert"
        date_match = re.search(r'[0-9]{2}\.[0-9]{2}\.[0-9]{4}', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'([0-9]{2})\.([0-9]{2})\.([0-9]{4})', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = int(month)
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

