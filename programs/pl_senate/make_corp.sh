#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp pl_senate.reg /corpora/registry/pl_senate

cat data/downloaded/* | $(ca_getpipeline pl) | 7zpipe /corpora/vert/parliament/pl_senate.vert.7z
compilecorp pl_senate --recompile-corpus
../deploy_to_beta.sh pl_senate