#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp lv_deputies.reg /corpora/registry/lv_deputies

cat data/downloaded/* | $(ca_getpipeline lv) | 7zpipe /corpora/vert/parliament/lv_deputies.vert.7z
compilecorp lv_deputies --recompile-corpus
../deploy_to_beta.sh lv_deputies