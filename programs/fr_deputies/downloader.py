# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import datetime
import re

remaining_urls = ["https://www.assemblee-nationale.fr/dyn/15/comptes-rendus/seance?limit=5&page=1"]

month_table = {
    "janvier": 1,
    "février": 2,
    "fevrier": 2,
    "mars": 3,
    "avril": 4,
    "mai": 5,
    "juin": 6,
    "juillet": 7,
    "août": 8,
    "aout": 8,
    "septembre": 9,
    "octobre": 10,
    "novembre": 11,
    "décembre": 12,
    "decembre": 12,
    "januari": 1,
    "februari": 2,
    "maart": 3,
    "april": 4,
    "mei": 5,
    "juni": 6,
    "juli": 7,
    "augustus": 8,
    "september": 9,
    "oktober": 10,
    "november": 11,
    "december": 12,
}

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if re.match(r'https://www.assemblee-nationale.fr/dyn/[^/]+/comptes-rendus/seance\?limit=.*', url) is not None:
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        href_found = False
        for element in elements:
            if element.get("href") is not None and re.match(r'/dyn/[^/]+/comptes-rendus/seance/.*', element.get("href")) is not None:
                href_found = True
                new_url = "https://www.assemblee-nationale.fr" + element.get("href")
                url_splited = new_url.split("#")
                if len(url_splited) > 1:
                    new_url = "#".join(url_splited[:-1])
                remaining_urls.append(new_url)
        if href_found:
            page_number = int(url.split("=")[-1]) + 1
            if "Aucun compte rendu de séance ne correspond aux critères de recherche" not in response.text:
                remaining_urls.append("=".join(url.split("=")[:-1]) + "=%s" % (page_number, ))
            if page_number == 2:
                seance = int(re.sub(r"https://www.assemblee-nationale.fr/dyn/([^/]+)/comptes-rendus/seance\?.*", "\\1", url))
                remaining_urls.append("https://www.assemblee-nationale.fr/dyn/%s/comptes-rendus/seance?limit=5&page=1" % (seance + 1, ))
    elif re.match(r'https://www.assemblee-nationale.fr/dyn/[^/]+/comptes-rendus/seance/[^/]+/.+', url) is not None and not url.endswith(".pdf"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        filename = url.split("/")[-1] + ".prevert"
        day, month, year = re.sub(r'.*(..)-([a-z]+)-(....).prevert', "\\1 \\2 \\3", filename).split()
        day = int(day)
        month = month_table[month]
        year = int(year)
        date = "%d-%02d-%02d" % (year, month, day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

