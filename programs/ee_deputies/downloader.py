# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
from bs4 import BeautifulSoup
import datetime
import re

remaining_urls = ["https://stenogrammid.riigikogu.ee/et?rangeFrom=01.01.%s&rangeTo=31.12.%s&singleDate=&phrase=&type=ALL&page=1" % (str(int(datetime.datetime.utcnow().strftime("%Y")) - 2), str(int(datetime.datetime.utcnow().strftime("%Y")) - 2))]


def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://stenogrammid.riigikogu.ee/et?rangeFrom="):
        response = requests.get(url, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        elements = page.find_all("a")
        href_found = False
        for element in elements:
            if element.get("href") is not None and element.get("href").startswith("/et/"):
                href_found = True
                href = element.get("href")
                href = href.split("#")
                if len(href) > 1:
                    href = href[:-1]
                href = "#".join(href)
                remaining_urls.append("https://stenogrammid.riigikogu.ee" + href)
        if href_found:
            page_number = int(url.split("=")[-1]) + 1
            remaining_urls.append("=".join(url.split("=")[:-1]) + "=%s" % (page_number, ))
            if page_number == 2:
                year = int(re.sub("https://stenogrammid\\.riigikogu\\.ee/et\\?rangeFrom=01\\.01\\.(....)&rangeTo=.*&singleDate=&phrase=&type=ALL&page=.*", "\\1", url))
                remaining_urls.append("https://stenogrammid.riigikogu.ee/et?rangeFrom=01.01.%s&rangeTo=31.12.%s&singleDate=&phrase=&type=ALL&page=1" % (year + 1, year + 1))
    elif url.startswith("https://stenogrammid.riigikogu.ee/et/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url)
        filename = url.split("/")[-1] + ".prevert"
        date = re.sub(r'^(....)(..)(..)....\.prevert$', "\\1-\\2-\\3", filename)
        year, month, day = date.split("-")
        year = int(year)
        month = int(month)
        day = int(day)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

