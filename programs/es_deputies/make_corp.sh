#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

./zipper.sh
cp es_deputies.reg /corpora/registry/es_deputies

find data/downloaded/ | ./zipcat.py | $(ca_getpipeline es) | 7zpipe /corpora/vert/parliament/es_deputies.vert.7z
compilecorp es_deputies --recompile-corpus
../deploy_to_beta.sh es_deputies