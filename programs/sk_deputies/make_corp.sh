#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp sk_deputies.reg /corpora/registry/sk_deputies

cat data/downloaded/* | $(ca_getpipeline sk) | 7zpipe /corpora/vert/parliament/sk_deputies.vert.7z
compilecorp sk_deputies --recompile-corpus
../deploy_to_beta.sh sk_deputies