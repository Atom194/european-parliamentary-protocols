# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, IO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
import xml.etree.ElementTree as ET
import re
from xml.etree.ElementTree import Element

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata


    def write(self, xml_file: IO) -> None:
        
        stenoprotocol_xml = ET.parse(xml_file)
        year, month, day = re.sub(r'([0-9]{4})\.([0-9]{2})\.([0-9]{2})\.', "\\1 \\2 \\3", stenoprotocol_xml.getroot().attrib["datum"]).split()
        self.doc_metadata["date_day"] = day
        self.doc_metadata["date_month"] = month
        self.doc_metadata["date_year"] = year
        self.doc_metadata["time"] = "===NONE==="
        self.doc_metadata["date"] = "%s-%s-%s" % (year, month, day)
        
        text = ""

        for element in stenoprotocol_xml.getroot():
            if element.tag == "felsz_ideje":
                self.doc_metadata["time"] = element.text.strip()
            elif element.tag == "felsz_szovege":
                if element.text is not None:
                    text = element.text
            elif element.tag == "felszolalo":
                self.last_speaker = element.text


        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")
        if self.last_speaker is not None:
            self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
        text = text.strip()
        if len(text) > 0:
            self.file.write("<p>\n")
            self.file.write(text)
            self.file.write("\n</p>\n")

    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)