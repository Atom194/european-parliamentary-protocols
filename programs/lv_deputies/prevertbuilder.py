# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'.\.\s?[^\s]+\s?\(.*\)\.')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("div", {'class': 'text'}):
            for element2 in element.find_all("p"):
                element2_str: str = element2.text
                element2_str = element2_str.strip()
                speaker_match = re.match(re_speaker, element2_str)
                if speaker_match is not None:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = element2_str
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                    continue
                element2_str = element2_str.replace("\n", " ")
                element2_str = element2_str.strip()
                if len(element2_str) > 0:
                    self.file.write("<p>\n" + element2.text + "\n</p>\n")


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
