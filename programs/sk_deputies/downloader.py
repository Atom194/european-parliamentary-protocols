# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime

URL_SEARCH_PREFIX = "https://www.nrsr.sk/dl/Search/Grid?legId=13&termNr=0&chamberId=0&categoryId=0&documentTypeId=5&dateFrom=%s-01-01&dateTo=%s-12-31&billNr=&text=&pageIndex=" % (int(datetime.datetime.now().strftime("%Y")) - 2, int(datetime.datetime.now().strftime("%Y")))

re_zip_filename = re.compile("s[0-9]{6}.htm")

remaining_urls = [{"url": URL_SEARCH_PREFIX + "0", "type": "search"}]

def process_url(crawler: Crawler,  url: Any) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url["url"], ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url["url"], ), LOG_TYPE.PRINT)
    if url["type"] == "search":
        response = requests.get(url["url"], verify=False, timeout=1200)
        page = BeautifulSoup(response.text, "html.parser")
        match_found = False
        for element in page.find_all("a"):
            if element.get("href") is not None and element.get("href").startswith("/dl/Browser/Document?documentId=") and "schôdza" in element.text:
                match_found = True
                element_str = element.text
                element_str = element_str.strip()
                year, month, day = re.sub(r'.*\s([0-9]{1,2})\.([0-9]{1,2})\.([0-9]{4})', "\\3 \\2 \\1", element_str).split()
                year = int(year)
                month = int(month)
                day = int(day)
                date = "%d-%02d-%02d" % (year, month, day)
                remaining_urls.append({"url": "https://www.nrsr.sk" + element.get("href"), "type": "steno", "date": date, "year": year, "month": month, "day": day})
        if match_found:
            current_page = int(re.sub(r'.*billNr=&text=&pageIndex=([0-9]+)', "\\1", url["url"]))
            remaining_urls.append({"url": URL_SEARCH_PREFIX + str(current_page + 1), "type": "search"})
    elif url["type"] == "steno":
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url["url"], verify=False, timeout=1200)
        if response.headers["Content-Type"] == "text/html; charset=utf-8":
            page = BeautifulSoup(response.text, "html.parser")
            for element in page.find_all("a"):
                if element.get("href") is not None and element.get("href").startswith("/dl/Browser/DsDocument?documentId="):
                    remaining_urls.append({"url": "https://www.nrsr.sk" + element.get("href"), "type": "steno", "date": url["date"], "year": url["year"], "month": url["month"], "day": url["day"]})
            return None
        if response.headers["Content-Type"] != "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            crawler.log("Unrecognized header: %s" % (response.headers["Content-Type"], ), LOG_TYPE.WARNING)
            return None
        filename = re.sub(r'.*\?documentId=([0-9]+)', "\\1.prevert", url["url"])
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url["url"], "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": url["date"], "date_day": str(url["day"]), "date_month": str(url["month"]), "date_year": str(url["year"])})   
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None       

def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url["url"], filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

