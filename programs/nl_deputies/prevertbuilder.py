# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile(r'^[^\:]*<strong>[^\:]*<\/strong>[^\:]*\:')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.is_final_document = True
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("p"):
            element_str = str(element)
            for line in element_str.split("<br>"):
                line = re.sub(r'\s+', " ", line)
                line = line.strip()
                speaker_match = re.search(re_speaker, line)
                if speaker_match is not None:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = element_str[:speaker_match.end()-1]
                    element_str = element_str[speaker_match.end():]
                    self.last_speaker = re.sub(r'<[^>]+>', "", self.last_speaker)
                    self.last_speaker = self.last_speaker.strip()
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                element_str = re.sub(r'<[^>]+>', "", element_str)
                element_str = element_str.strip()
                if len(element_str) > 0:
                    self.file.write("<p>\n")
                    self.file.write(element_str)
                    self.file.write("\n</p>\n")

    
    def build(self):
        if not self.is_final_document:
            self.file.close()
            import os
            os.remove(self.crawler.dir_unconfirmed + "/" + self.filename)
            raise AssertionError("This document is not corected. Try again later.")
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)