# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    
    def _write_rec(self, elements):
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        for element in elements.find_all(recursive=False):
            if element.name == "b":
                if len(element.text.strip()) > 0 and ":" == element.text.strip()[-1]:
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = element.text[:-1]
                    self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                else:
                    self.file.write("<p>\n" + element.text.strip() + "\n</p>\n")
            elif element.name == "font":
                self.file.write("<p>\n" + element.text.strip() + "\n</p>\n")
            elif element.name == "br":
                continue
            elif element.name == "h5":
                continue
            elif element.name == "div":
                if element.get("align") is not None and element.get("align") == 'center':
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.file.write("<p>\n" + element.text + "\n</p>\n")
                    if self.last_speaker is not None:
                        self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                else:
                    unchatched_element(element)
            elif element.name == "p":
                self.file.write("<p>\n" + element.text + "\n</p>\n")
            elif element.name == "sub":
                element_str = element.text.strip()
                if len(element_str) == 0:
                    continue
                else:
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
            elif element.name == "sup":
                element_str = element.text.strip()
                if len(element_str) == 0:
                    continue
                else:
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
            elif element.name == "a":
                element_str = element.text.strip()
                if len(element_str) == 0:
                    continue
                else:
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
            elif element.name == "ul":
                element_str = element.text.strip()
                if len(element_str) == 0:
                    continue
                else:
                    self.file.write("<p>\n" + element_str + "\n</p>\n")
            else:
                unchatched_element(element)


    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("div", {"class": "fieldset"}):
            self._write_rec(element)


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
