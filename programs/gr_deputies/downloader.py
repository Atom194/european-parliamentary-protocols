# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime
import time

TIME_SLEEP_SECONDS = 1
URL_SEARCH_PREFIX = "https://www.hellenicparliament.gr/praktika/synedriaseis-olomeleias?pageNo="

HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'}

remaining_urls = [{"url": URL_SEARCH_PREFIX + "1", "type": "search"}]

def process_url(crawler: Crawler,  url: Any) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url["url"], ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url["url"], ), LOG_TYPE.PRINT)
    if url["type"] == "search":
        time.sleep(TIME_SLEEP_SECONDS)
        response = requests.get(url["url"], verify=False, timeout=1200, headers=HEADERS)
        page = BeautifulSoup(response.text, "html.parser")
        for element2 in page.find_all("tr"):
            for element in element2.find_all("a"):
                if element.get("href") is not None and element.get("href").startswith("https://www.hellenicparliament.gr/UserFiles/") and element.get("href").endswith(".docx"):
                    element_str = element.text
                    element_str = element_str.strip()
                    date = re.sub(r'.*>\s*([0-9]{2})/([0-9]{2})/([0-9]{4})\s*(\([^\)]+\))?\s*<.*', "\\3-\\2-\\1", str(element2).replace("\n", ""))
                    year, month, day = date.split("-")
                    year = int(year)
                    month = int(month)
                    day = int(day)
                    date = "%d-%02d-%02d" % (year, month, day)
                    remaining_urls.append({"url": element.get("href"), "type": "steno", "date": date, "year": year, "month": month, "day": day})
        remaining_pages = re.sub(r'.*Σελίδα ([0-9]+) από ([0-9]+).*', "\\1 \\2", str(page).replace("\n", "")).split()
        if remaining_pages[0] != remaining_pages[1]:
            current_page = int(re.sub(r'.*/synedriaseis-olomeleias\?pageNo=([0-9]+)', "\\1", url["url"]))
            if current_page < 10:
                remaining_urls.append({"url": URL_SEARCH_PREFIX + str(current_page + 1), "type": "search"})
    elif url["type"] == "steno":
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        time.sleep(TIME_SLEEP_SECONDS)
        response = requests.get(url["url"], verify=False, timeout=1200, headers=HEADERS)
        filename = re.sub(r'.*/([^/]+).docx$', "\\1.prevert", url["url"])
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url["url"], "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": url["date"], "date_day": str(url["day"]), "date_month": str(url["month"]), "date_year": str(url["year"])}) 
        prevert_builder.write(io.BytesIO(response.content))
        prevert_builder.build()
        return filename
    return None       

def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url["url"], filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

