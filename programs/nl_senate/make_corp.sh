#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp nl_senate.reg /corpora/registry/nl_senate

cat data/downloaded/* | $(ca_getpipeline nl) | 7zpipe /corpora/vert/parliament/nl_senate.vert.7z
compilecorp nl_senate --recompile-corpus
../deploy_to_beta.sh nl_senate