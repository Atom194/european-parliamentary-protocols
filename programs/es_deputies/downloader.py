# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
from bs4 import BeautifulSoup
import requests
import io
import shutil
import datetime
import re
import zipfile

def int_to_roman(number: int) -> str:
    lab = {}
    lab[1000] = "M"
    lab[900] = "CM"
    lab[500] = "D"
    lab[400] = "CD"
    lab[100] = "C"
    lab[90] = "XC"
    lab[50] = "L"
    lab[40] = "XL"
    lab[10] = "X"
    lab[9] = "IX"
    lab[5] = "V"
    lab[4] = "IV"
    lab[1] = "I"
    roman_number = ""
    while number > 0:
        for key in lab:
            roman_number += lab[key] * (number // key)
            number = number % key
    return roman_number

HEADERS = {}
HEADERS["referer"] = "https://www.congreso.es/es/busqueda-de-publicaciones"
HEADERS["user-agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"

remaining_urls = [{"url": "https://www.congreso.es/es/busqueda-de-publicaciones?p_p_id=publicaciones&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=filtrarListado&p_p_cacheability=cacheLevelPage",
                   "data": {
                        "_publicaciones_legislatura": 5,
                        "_publicaciones_texto": "",
                        "_publicaciones_tipoBusqueda": 0,
                        "_publicaciones_publicacion": "D",
                        "_publicaciones_seccion": "",
                        "_publicaciones_serie": "",
                        "_publicaciones_fechaDesde": "",
                        "_publicaciones_fechaHasta": "",
                        "_publicaciones_numeroDesde": "",
                        "_publicaciones_numeroHasta": "",
                        "_publicaciones_subindice": "",
                        "_publicaciones_descripcion": "",
                        "_publicaciones_paginaActual": 1,
                   },
                   "type": "search",
                   }]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url["url"], ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url["type"] == "search":
        response = requests.post(url["url"], data=url["data"], headers=HEADERS)
        response_json = response.json()
        if "error" in response_json:
            if "Error en la consulta" in response_json["error"]:
                return
            else:
                crawler.log("Error response %s in request %s.", (response_json, url), LOG_TYPE.WARNING)
        for key in response_json:
            if key in ["legislatura", "paginaActual", "publicaciones_encontradas", "paginacion"]:
                continue
            remaining_urls.insert(0, {
                "url": "https://www.congreso.es/es/busqueda-de-publicaciones?p_p_id=publicaciones&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&_publicaciones_mode=mostrarTextoIntegro&_publicaciones_legislatura=%s&_publicaciones_texto=&_publicaciones_id_texto=" % (int_to_roman(url["data"]["_publicaciones_legislatura"]), )+ response_json[key]["texto_integro"].split("+")[-1],
                "metadata": response_json[key],
                "type": "steno"
            })
        max_page_number = int(response_json["publicaciones_encontradas"]) // 20 + 1
        if url["data"]["_publicaciones_paginaActual"] < max_page_number:
            remaining_urls.append({"url": "https://www.congreso.es/es/busqueda-de-publicaciones?p_p_id=publicaciones&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=filtrarListado&p_p_cacheability=cacheLevelPage",
                   "data": {
                        "_publicaciones_legislatura": url["data"]["_publicaciones_legislatura"],
                        "_publicaciones_texto": "",
                        "_publicaciones_tipoBusqueda": 0,
                        "_publicaciones_publicacion": "D",
                        "_publicaciones_seccion": "",
                        "_publicaciones_serie": "",
                        "_publicaciones_fechaDesde": "",
                        "_publicaciones_fechaHasta": "",
                        "_publicaciones_numeroDesde": "",
                        "_publicaciones_numeroHasta": "",
                        "_publicaciones_subindice": "",
                        "_publicaciones_descripcion": "",
                        "_publicaciones_paginaActual": url["data"]["_publicaciones_paginaActual"] + 1,
                   },
                   "type": "search",
                   })
            if url["data"]["_publicaciones_paginaActual"] == max_page_number -1:
                remaining_urls.append({"url": "https://www.congreso.es/es/busqueda-de-publicaciones?p_p_id=publicaciones&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=filtrarListado&p_p_cacheability=cacheLevelPage",
                                    "data": {
                                            "_publicaciones_legislatura": url["data"]["_publicaciones_legislatura"] + 1,
                                            "_publicaciones_texto": "",
                                            "_publicaciones_tipoBusqueda": 0,
                                            "_publicaciones_publicacion": "D",
                                            "_publicaciones_seccion": "",
                                            "_publicaciones_serie": "",
                                            "_publicaciones_fechaDesde": "",
                                            "_publicaciones_fechaHasta": "",
                                            "_publicaciones_numeroDesde": "",
                                            "_publicaciones_numeroHasta": "",
                                            "_publicaciones_subindice": "",
                                            "_publicaciones_descripcion": "",
                                            "_publicaciones_paginaActual": 1,
                                    },
                                    "type": "search",
                                    })

    elif url["type"] == "steno":
        crawler.log("Downloading: %s" % (url["url"], ), LOG_TYPE.MESSAGE)
        response = requests.get(url["url"], headers=HEADERS, timeout=1200)
        filename = url["metadata"]["texto_integro"].split("+")[-1].split(".#")[0] + ".prevert"
        url["metadata"]["source_url"] = url["url"]
        url["metadata"]["url_access_time"] = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")
        url["metadata"]["filename"] = filename
        url["metadata"]["date"] = re.sub(r'(....)(..)(..)', "\\1-\\2-\\3", url["metadata"]["fecha"])
        url["metadata"]["date_year"], url["metadata"]["date_month"], url["metadata"]["date_day"] = url["metadata"]["date"].split("-")
        url["metadata"]["date_year"] = str(int(url["metadata"]["date_year"]))
        url["metadata"]["date_month"] = str(int(url["metadata"]["date_month"]))
        url["metadata"]["date_day"] = str(int(url["metadata"]["date_day"]))
        url["metadata"].pop("fecha", None)
        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata=url["metadata"])
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url["url"], filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

