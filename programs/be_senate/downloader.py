# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import zipfile, io
import shutil
from bs4 import BeautifulSoup
import re
import sqlite3
import datetime

month_tb = {
    "janvier": 1,
    "février": 2,
    "mars": 3,
    "avril": 4,
    "mai": 5,
    "juin": 6,
    "juillet": 7,
    "août": 8,
    "septembre": 9,
    "octobre": 10,
    "novembre": 11,
    "décembre": 12,

    "januari": 1,
    "februari": 2,
    "maart": 3,
    "april": 4,
    "mei": 5,
    "juni": 6,
    "juli": 7,
    "augustus": 8,
    "september": 9,
    "oktober": 10,
    "november": 11,
    "december": 12
}

re_zip_filename = re.compile("s[0-9]{6}.htm")
remaining_urls = ["https://senate.be/crv/7-1.html"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    legis, number = re.sub(r'https://senate\.be/crv/([0-9]+)-([0-9]+)\.html', "\\1 \\2", url).split()
    legis = int(legis)
    number = int(number)
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        if number == 1:
            remaining_urls.append("https://senate.be/crv/%s-1.html" % (legis + 1, ))
        remaining_urls.append("https://senate.be/crv/%s-%s.html" % (legis, number + 1))
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    response = requests.get(url, verify=False, timeout=1200)
    if response.status_code == 404:
        return None
    elif response.status_code != 200:
        crawler.log("Invalid status code %s." % (response.status_code, ), LOG_TYPE.WARNING)
        return None
    if number == 1:
        remaining_urls.append("https://senate.be/crv/%s-1.html" % (legis + 1, ))
    remaining_urls.append("https://senate.be/crv/%s-%s.html" % (legis, number + 1))
    crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
    filename = re.sub(r'https://senate\.be/crv/([0-9]+)-([0-9]+)\.html', "\\1_\\2.prevert", url)
    try:
        page = BeautifulSoup(response.content.decode("utf-8"), "html.parser")
    except UnicodeDecodeError:
        page = BeautifulSoup(response.content.decode("utf-16"), "html.parser")
    element_str = str(page.find("td"))
    element_str = re.sub(r'\s+', " ", element_str)
    day, month, year = re.sub(r'.*>[^:\-]+ ([0-9]{1,2}) ([^:\-]+) ([0-9]{4})<.*', "\\1 \\2 \\3", element_str).split()
    month = month_tb[month]
    prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={
                                                            "source_url": url,
                                                            "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"),
                                                            "filename": filename,
                                                            "date": "%s-%02d-%02d" % (year, month, int(day)),
                                                            "date_year": year,
                                                            "date_month": str(month),
                                                            "date_day": day,
                                                        })
    try:
        stream = io.StringIO(response.content.decode("utf-8"))
    except UnicodeDecodeError:
        stream = io.StringIO(response.content.decode("utf-16"))
    prevert_builder.write(stream)
    prevert_builder.build()
    return filename


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        last_url = crawler.dbcursor.execute("SELECT url FROM downloaded ORDER BY id DESC LIMIT 1").fetchall()

        if len(last_url) != 0:
            global remaining_urls
            remaining_urls = []
            legislative, doc_id = re.sub(r'https://senate\.be/crv/([0-9]+)-([0-9]+)\.html', "\\1 \\2", last_url[0][0]).split()
            legislative = int(legislative)
            doc_id = int(doc_id)
            remaining_urls.append("https://senate.be/crv/%s-1.html" % (str(legislative + 1)))
            remaining_urls.append("https://senate.be/crv/%s-%s.html" % (str(legislative), str(doc_id + 1)))

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except Exception as e:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

