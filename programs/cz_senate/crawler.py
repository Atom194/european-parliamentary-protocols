# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List
from datetime import datetime
from enum import Enum
import os, sys
import sqlite3
from mail import Mail

DATA_LOCK_FILE = "data.lock"
LOG_FILE = "log.txt"

class LOG_TYPE(Enum):
    PRINT = "Print"
    MESSAGE = "Message"
    WARNING = "Warning"
    ERROR = "Error"

class Crawler:
    log_file: Optional[TextIO] = None
    dbcursor: Optional[sqlite3.Connection] = None

    def __init__(self, work_dir: str = "./data/", parlament_name: str = "Undef", smtp_servers: List[str] = ["localhost"], mail_from: str = "crawler@localhost", mail_recepients: List[str] = ["root@localhost"], log_size: int = 1000000000) -> None:
        self.mail_waring = Mail(smtp_servers, mail_recepients, mail_from)
        self.mail_error = Mail(smtp_servers, mail_recepients, mail_from)
        self.work_dir = os.path.abspath(work_dir)
        self.parlament_name = parlament_name
        self.log_size = log_size
        try:
            os.mkdir(self.work_dir)
        except FileExistsError:
            pass

        self.dir_downloaded = os.path.normpath(work_dir + "/downloaded")
        try:
            os.mkdir(self.dir_downloaded)
        except FileExistsError:
            pass

        self.dir_unconfirmed = os.path.normpath(work_dir + "/unconfirmed")
        try:
            os.mkdir(self.dir_unconfirmed)
        except FileExistsError:
            pass


    def lock_data(self) -> None:
        try:
            fd_lock = os.open(self.work_dir + "/" + DATA_LOCK_FILE, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
            os.write(fd_lock, str(os.getpid()).encode("ascii"))
            os.close(fd_lock)
        except FileExistsError as e:
            with open(self.work_dir + "/" + DATA_LOCK_FILE, "r") as pid_lock:
                pid = pid_lock.read()
            raise FileExistsError("Data folder is locked by process: %s. If you want to run the program anyway, make sure this process is not running and delete file '%s'. Error: %s" % (pid, self.work_dir + DATA_LOCK_FILE, str(e)))

    def unlock_data(self) -> None:
        try:
            os.remove(self.work_dir + "/" + DATA_LOCK_FILE)
        except FileNotFoundError as e:
            raise FileNotFoundError("Unable to release data lock. Lock was not released by this program, but probably by some other program. Error: %s" % (str(e)))

    def start_log(self):
        self.log_file = open(self.work_dir + "/" + LOG_FILE, "a")

    def rotate_log(self) -> None:
        if os.path.getsize(self.work_dir + "/" + LOG_FILE) > self.log_size:
            self.stop_log()
            os.rename(self.work_dir + "/" + LOG_FILE, self.work_dir + "/" + LOG_FILE + ".old")
            self.start_log()

    def log(self, message: str, log_type: LOG_TYPE = LOG_TYPE.MESSAGE):
        """
        Logs messages in priority order:
        1. print on stdout/stderr
        2. write in log file
        3. send mail.
        """
        self.rotate_log()

        if message[-1] != "\n":
            message += "\n"
        data_to_log = "[%s][%s] %s" % (datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), log_type.value, message)

        if log_type == LOG_TYPE.ERROR or log_type == LOG_TYPE.WARNING:
            print(data_to_log, file=sys.stderr, end="")
        else:
            print(data_to_log, end="")

        if log_type == LOG_TYPE.MESSAGE or log_type == LOG_TYPE.WARNING or log_type == LOG_TYPE.ERROR:
            self.log_file.write(data_to_log)
            self.log_file.flush()

        if log_type == LOG_TYPE.ERROR:
            self.mail_error.notify("Error: " + self.parlament_name, data_to_log + "\n\nPossibly more errors encountred. Check logs!")
        elif log_type == LOG_TYPE.WARNING:
            self.mail_error.notify("Warning: " + self.parlament_name, data_to_log + "\n\nPossibly more warnings encountred. Check logs!")
        

    def stop_log(self):
        if self.log_file:
            self.log_file.close()

    def start(self) -> None:
        self.lock_data()
        self.start_log()
        self.log("Process %s accessed. Lock created." % (os.getpid()))
        self.dbcursor = sqlite3.connect(self.work_dir + "/downloaded.db")
        self.log("Cursor to DB created.")

    def stop(self) -> None:
        self.dbcursor.close()
        self.log("Cursor to DB closed.")
        self.log("Process %s left. Releasing lock." % (os.getpid()))
        self.stop_log()
        self.unlock_data()