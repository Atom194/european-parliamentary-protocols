# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
import json

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata


    def write(self, json_file: IO, subfilename = "") -> None:
        json_binary = json_file.read()
        try:
            speach_json = json.loads(json_binary.decode("utf-8"))
        except json.decoder.JSONDecodeError:
            # self.crawler.log("Ignoring errors in decode for file %s in %s" % (subfilename, self.filename), LOG_TYPE.WARNING)
            speach_json = json.loads(json_binary.decode("utf-8-sig"))

        date = speach_json["anforande"].get('dok_datum', "===NONE===").split()[0]
        if date == "===NONE===":
            year = "===NONE==="
            month = "===NONE==="
            day = "===NONE==="
        else:
            year, month, day = date.split("-")
            year = int(year)
            month = int(month)
            day = int(day)

        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(' subfilename="%s"' % (subfilename, ))
        self.file.write(' doc_hangar_id="%s"' % (speach_json["anforande"].get('dok_hangar_id', "===NONE==="), ))
        self.file.write(' doc_id="%s"' % (speach_json["anforande"].get('dok_id', "===NONE==="), ))
        self.file.write(' doc_titel="%s"' % (speach_json["anforande"].get('dok_titel', "===NONE==="), ))
        self.file.write(' doc_rm="%s"' % (speach_json["anforande"].get('dok_rm', "===NONE==="), ))
        self.file.write(' doc_nummer="%s"' % (speach_json["anforande"].get('dok_nummer', "===NONE==="), ))
        self.file.write(' date="%s"' % (date, ))
        self.file.write(' date_year="%s"' % (str(year), ))
        self.file.write(' date_month="%s"' % (str(month), ))
        self.file.write(' date_day="%s"' % (str(day), ))
        self.file.write(' title_of_section="%s"' % (speach_json["anforande"].get('avsnittsrubrik', "===NONE==="), ))
        self.file.write(' title_subsection="%s"' % (speach_json["anforande"].get('underrubrik', "===NONE==="), ))
        self.file.write(' chamber_activity="%s"' % (speach_json["anforande"].get('kammaraktivitet', "===NONE==="), ))
        self.file.write(' request_number="%s"' % (speach_json["anforande"].get('anforande_nummer', "===NONE==="), ))
        self.file.write(' stakeholder_id="%s"' % (speach_json["anforande"].get('intressent_id', "===NONE==="), ))
        self.file.write(' rel_dok_id="%s"' % (speach_json["anforande"].get('rel_dok_id', "===NONE==="), ))
        self.file.write(' replica="%s"' % (speach_json["anforande"].get('replik', "===NONE==="), ))
        self.file.write(' system_date="%s"' % (speach_json["anforande"].get('systemdatum', "===NONE==="), ))
        self.file.write(">\n")

        self.file.write("<speaker")
        self.file.write(' name="%s"' % (speach_json["anforande"].get('talare', "===NONE==="), ))
        self.file.write(' party="%s"' % (speach_json["anforande"].get('parti', "===NONE==="), ))
        self.file.write(">\n")

        speach_str = speach_json["anforande"]["anforandetext"]
        if speach_str is None:
            self.file.write("===NONE===\n")
        else:
            speach_str = re.sub(r'<p> *', "<p>\n", speach_str)
            speach_str = re.sub(r'</p> *', "\n</p>\n", speach_str)
            speach_str = speach_str.strip() + "\n"
            self.file.write(speach_str)

        self.file.write("</speaker>\n")
        self.file.write("</doc>\n")


    def build(self):
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
