# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, BinaryIO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile(r'<span[^>]*class="orateur_nom"[^>]*>[^>]*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")


    def write(self, html_file: BinaryIO) -> None:
        page = BeautifulSoup(html_file.read().decode("utf-8"), "html.parser")
        elements = page.find_all('div', {'class': 'db-section'})
        for element in elements:
            for element2 in element.find_all(recursive=False):
                if element2.name == "div":
                    if element2.get("class") is not None and element2.get("class") == ["speech"]:
                        for element3 in element2.find_all(recursive=False):
                            if "c-avatar" in element3.get("class"):
                                if self.last_speaker is not None:
                                    self.file.write("</speaker>\n")
                                self.last_speaker = re.sub(r'</?a[^>]*>', "", str(element3))
                                self.last_speaker = re.sub(r'</?div[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?h4[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?i[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?span[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = self.last_speaker.strip()
                                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                            elif "text" in element3.get("class"):
                                element3_str = set(element3)
                                element3_str = re.sub(r'</?a[^>]*>', "", str(element3_str))
                                element3_str = re.sub(r'</?div[^>]*>', "", str(element3_str))
                                element3_str = re.sub(r'</?i[^>]*>', "", str(element3_str))

                                element3_str = re.sub(r'^{Share\n', "", str(element3_str))
                                element3_str = re.sub(r'}$', "", str(element3_str))
                                element3_str = re.sub(r'<p[^>]*>', "<p>\n", str(element3_str))
                                element3_str = re.sub(r'</p[^>]*>', "\n</p>", str(element3_str))
                                element3_str = re.sub(r'<p>\n ', "<p>\n", str(element3_str))

                                self.file.write(element3_str)
                    elif element2.get("class") is not None and element2.get("class") == ["speech", "brief"]:
                        for element3 in element2.find_all(recursive=False):
                            if "c-avatar" in element3.get("class"):
                                if self.last_speaker is not None:
                                    self.file.write("</speaker>\n")
                                self.last_speaker = re.sub(r'</?a[^>]*>', "", str(element3))
                                self.last_speaker = re.sub(r'</?div[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?h4[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?i[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = re.sub(r'</?span[^>]*>', "", str(self.last_speaker))
                                self.last_speaker = self.last_speaker.strip()
                                self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))
                            elif "text" in element3.get("class"):
                                element3_str = set(element3)
                                element3_str = re.sub(r'</?a[^>]*>', "", str(element3_str))
                                element3_str = re.sub(r'</?div[^>]*>', "", str(element3_str))
                                element3_str = re.sub(r'</?i[^>]*>', "", str(element3_str))

                                element3_str = re.sub(r'^{Share\n', "", str(element3_str))
                                element3_str = re.sub(r'}$', "", str(element3_str))
                                element3_str = re.sub(r'<p[^>]*>', "<p>\n", str(element3_str))
                                element3_str = re.sub(r'</p[^>]*>', "\n</p>", str(element3_str))
                                element3_str = re.sub(r'<p>\n ', "<p>\n", str(element3_str))

                                self.file.write(element3_str)
                    elif element2.get("class") is not None and element2.get("class") == ["summary"]:
                        element2_str = str(element2)
                        element2_str = re.sub(r'</?div[^>]*>', "", str(element2_str))
                        element2_str = re.sub(r'</?i[^>]*>', "", str(element2_str))
                        element2_str = element2_str.strip()

                        if self.last_speaker is not None:
                            self.file.write("</speaker>\n")
                        self.file.write('<note type="Other">\n')
                        self.file.write(element2_str)
                        self.file.write('\n</note>\n')
                        if self.last_speaker is not None:
                            self.file.write('<speaker name="%s">\n' % (self.last_speaker, ))


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)