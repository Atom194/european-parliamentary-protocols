#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp at_deputies.reg /corpora/registry/at_deputies

cat data/downloaded/* | $(ca_getpipeline de) | 7zpipe /corpora/vert/parliament/at_deputies.vert.7z
compilecorp at_deputies --recompile-corpus
../deploy_to_beta.sh at_deputies