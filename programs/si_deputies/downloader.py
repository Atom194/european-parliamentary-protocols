# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
import datetime
import xml
import xml.etree.ElementTree as ET
import re

month_table = {
    "januar": 1,
    "februar": 2,
    "marec": 3,
    "april": 4,
    "maj": 5,
    "junij": 6,
    "junija": 6,
    "julij": 7,
    "avgust": 8,
    "september": 9,
    "oktober": 10,
    "november": 11,
    "december": 12,
}

remaining_urls = ["https://fotogalerija.dz-rs.si/datoteke/opendata/SDZ.XML"]

def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        return None
    crawler.log("Resolving URL: %s" % (url, ), LOG_TYPE.PRINT)
    if url.startswith("https://fotogalerija.dz-rs.si/datoteke/opendata/SDZ"):
        response = requests.get(url, timeout=1200)
        if response.status_code != 200:
            if response.status_code != 404:
                crawler.log("Unable to access %s status code %s" % (url, str(response.status_code)), LOG_TYPE.MESSAGE)
            return None
        try:
            stenoprotocol_xml = ET.parse(io.StringIO(response.content.decode("utf-8"))).getroot()
        except xml.etree.ElementTree.ParseError as e:
            crawler.log("Parse missmatch: %s" % (str(e), ), LOG_TYPE.MESSAGE)
            return None
        for record in stenoprotocol_xml:
            if record.tag == "DOKUMENT":
                for card in record:
                    unid = card.find("UNID")
                    if unid.text.startswith("SDZ|"):
                        break
                    elif unid.text.startswith("MDZ|") or unid.text.startswith("SZA|"):
                        for new_url in card.findall("KARTICA_URL_MAGNETOGRAM"):
                            remaining_urls.append(new_url.text)
                    else:
                        crawler.log("Unmatched UNID: %s" % (unid.text, ), LOG_TYPE.WARNING)
        sdz_number = re.sub(r'https://fotogalerija.dz-rs.si/datoteke/opendata/SDZ([0-9]*).XML', "\\1", url)
        if len(sdz_number) == 0:
            sdz_number = 2
        else:
            sdz_number = int(sdz_number) + 1
        remaining_urls.append("https://fotogalerija.dz-rs.si/datoteke/opendata/SDZ%s.XML" % (sdz_number, ))
    elif url.startswith("http://www.dz-rs.si/wps/portal/Home/seje/"):
        crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
        response = requests.get(url, timeout=1200)
        filename = re.sub(r'http://www.dz-rs.si/wps/portal/Home/seje/evidenca\?mandat=(.+)&type=(.+)&uid=(.+)', "\\1_\\2_\\3.prevert", url)

        date_match = re.search(r'\([0-9]{1,2}\.?\s+[a-zA-Záéíúêó]+\s+[0-9]{4}\)', response.text)
        if date_match is None:
            day = "===NONE==="
            month = "===NONE==="
            year = "===NONE==="
            date = "===NONE==="
            crawler.log("Unable to find date for %s" % (url, ), LOG_TYPE.WARNING)
        else:
            date_text = response.text[date_match.start():date_match.end()]
            day, month, year = re.sub(r'\(([0-9]{1,2})\.?\s+([a-zA-Záéíúêó]+)\s+([0-9]{4})\)', "\\1 \\2 \\3", date_text).split()
            month = month.replace("&auml;", "ä")
            day = int(day)
            month = month_table[month]
            year = int(year)
            date = "%d-%02d-%02d" % (year, month, day)

        prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": url, "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "date": date, "date_day": str(day), "date_month": str(month), "date_year": str(year)})
        prevert_builder.write(io.StringIO(response.text))
        prevert_builder.build()
        return filename
    return None


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_urls) != 0:
            url = remaining_urls.pop(0)
            filename = process_url(crawler, url)
            if filename is not None:
                crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

