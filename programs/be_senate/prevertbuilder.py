# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, List, IO, Dict
from crawler import Crawler, LOG_TYPE
import shutil
from bs4 import BeautifulSoup
import re

re_speaker = re.compile('<span [^>]*style=[^>]*>.*</span>')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename_nl = filename + ".nl"
        self.filename_fr = filename + ".fr"
        self.last_speaker_nl: Optional[str] = None
        self.last_speaker_fr: Optional[str] = None
        self.file_nl: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename_nl, "w")
        self.file_fr: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename_fr, "w")
        self.doc_metadata = doc_metadata
        self.file_nl.write("<doc")
        self.file_fr.write("<doc")
        for key in self.doc_metadata:
            self.file_nl.write(' %s="%s"' % (key, self.doc_metadata[key]))
            self.file_fr.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file_nl.write(">\n")
        self.file_fr.write(">\n")

    def write(self, html_file: IO) -> None:
        def uncatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename_fr, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)

        page = BeautifulSoup(html_file.read(), "html.parser")
        for element in page.find_all("p"):
            for element2 in element.find_all("span"):
                if element2.get("lang") is not None and (element2.get("lang") == "FR" or element2.get("lang") == "NL"):
                    if (". -" in element.text or ". –" in element.text) and "b" in [x.name for x in element2.parents]:
                        if element2.get("lang") == "FR":
                            if self.last_speaker_fr is not None:
                                self.file_fr.write("</speaker>\n")
                            self.last_speaker_fr = element2.text
                            self.last_speaker_fr = re.sub(r'\s+', " ", self.last_speaker_fr)
                            self.last_speaker_fr = self.last_speaker_fr.strip()
                            self.file_fr.write('<speaker name="%s">\n' % (self.last_speaker_fr, ))
                        else:
                            if self.last_speaker_nl is not None:
                                self.file_nl.write("</speaker>\n")
                            self.last_speaker_nl = element2.text
                            self.last_speaker_nl = re.sub(r'\s+', " ", self.last_speaker_nl)
                            self.last_speaker_nl = self.last_speaker_nl.strip()
                            self.file_nl.write('<speaker name="%s">\n' % (self.last_speaker_nl, ))
                        continue
                    element2_str = element2.text
                    element2_str = re.sub(r'\s+', " ", element2_str)
                    element2_str = element2_str.strip()
                    if element2_str.startswith(". -") or element2_str.startswith(". –"):
                        element2_str = element2_str[3:]
                    element2_str = element2_str.strip()
                    if len(element2_str) == 0:
                        continue
                    if element2.get("lang") == "FR":
                        self.file_fr.write("<p>\n" + element2_str + "\n</p>\n")
                    else:
                        self.file_nl.write("<p>\n" + element2_str + "\n</p>\n")
                            
                else:
                    uncatched_element(element)
    
    def build(self):
        if len(self.doc_metadata) != 0:
            self.file_nl.write("</doc>")
            self.file_fr.write("</doc>")
        self.file_nl.close()
        self.file_fr.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename_nl, self.crawler.dir_downloaded + "/" + self.filename_nl)
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename_fr, self.crawler.dir_downloaded + "/" + self.filename_fr)