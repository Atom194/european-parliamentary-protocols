# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Optional, TextIO, Dict, IO
from crawler import Crawler, LOG_TYPE
import shutil
import re
from bs4 import BeautifulSoup, NavigableString

re_speaker = re.compile(r'^([^:<]*<[^>]*>)*[^:<]*:')

class PrevertBuilder:    
    def __init__(self, crawler: Crawler, filename: str, doc_metadata: Dict[str, str] = {}) -> None:
        self.crawler = crawler
        self.filename = filename
        self.last_speaker: Optional[str] = None
        self.file: Optional[TextIO] = open(crawler.dir_unconfirmed + "/" + self.filename, "w")
        self.doc_metadata = doc_metadata
        self.file.write("<doc")
        for key in self.doc_metadata:
            self.file.write(' %s="%s"' % (key, self.doc_metadata[key]))
        self.file.write(">\n")

    def _write_rec(self, recurse_element: BeautifulSoup) -> None:
        def unchatched_element(element):
            self.crawler.log("In file %s: Uncatched: %s -> %s" % (self.filename, [x.name + " %s" % (x.attrs) for x in element.parents][::-1], element.name + " %s" % (element.attrs)), LOG_TYPE.WARNING)
        
        def process_paragraf(element):

            element_str = str(element)
            double_dot = re.search(re_speaker, element_str)

            if double_dot is not None:
                speaker_text = element_str[:double_dot.end()-1]
                if "/WWER/PAD_" in speaker_text and "<a" in speaker_text:
                    role_split = speaker_text.find("<a")
                    if role_split >= 0:
                        role = speaker_text[:role_split]
                        role = re.sub(r'\s+', " ", role)
                        role = re.sub(r'<[^>]+>', "", role)
                        role = role.strip()
                        speaker_text = speaker_text[role_split:]
                    else:
                        role = "===NONE==="
                    element_str = element_str[double_dot.end():]
                    if self.last_speaker is not None:
                        self.file.write("</speaker>\n")
                    self.last_speaker = speaker_text
                    self.last_speaker = re.sub(r'\s+', " ", self.last_speaker)
                    self.last_speaker = re.sub(r'<[^>]+>', "", self.last_speaker)
                    self.last_speaker = self.last_speaker.strip()
                    self.file.write('<speaker name="%s" role="%s">\n' % (self.last_speaker, role))
            element_str = re.sub(r'\s+', " ", element_str)
            element_str = re.sub(r'<[^>]+>', "", element_str)
            element_str = element_str.strip()
            if len(element_str) > 0:
                self.file.write("<p>\n" + element_str + "\n</p>\n")
                

        for element in recurse_element.find_all(recursive=False):
            if element.name == "html":
                self._write_rec(element)
            elif element.name == "b" and [x.name for x in element.parents] == ["body", "html", "[document]"]:
                continue
            elif element.name == "a" and [x.name for x in element.parents] == ["div", "body", "html", "[document]"]:
                continue
            elif element.name == "hr":
                continue
            elif element.name == "i" and [x.name for x in element.parents] == ["body", "html", "[document]"]:
                continue
            elif element.name == "table" and [x.name for x in element.parents] == ["div", "div", "body", "html", "[document]"]:
                continue
            elif element.name == "body":
                if [x.name for x in element.parents] == ["html", "[document]"]:
                    self._write_rec(element)
            elif element.name == "head":
                continue
            elif element.name == "br":
                continue
            elif element.name == "span":
                self._write_rec(element)
            elif element.name == "div":
                self._write_rec(element)
            elif element.name == "p":
                if element.get("class") is not None and element.get("class") == ["MsoNormal"]:
                    process_paragraf(element)

            else:
                unchatched_element(element)

    def write(self, html_file: IO) -> None:
        page = BeautifulSoup(html_file.read(), "html.parser")
        self._write_rec(page)


    def build(self):
        if self.last_speaker is not None:
            self.file.write("</speaker>\n")
        if len(self.doc_metadata) != 0:
            self.file.write("</doc>")
        self.file.close()
        shutil.move(self.crawler.dir_unconfirmed + "/" + self.filename, self.crawler.dir_downloaded + "/" + self.filename)
