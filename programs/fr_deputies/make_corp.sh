#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp fr_deputies.reg /corpora/registry/fr_deputies

cat data/downloaded/* | $(ca_getpipeline fr) | 7zpipe /corpora/vert/parliament/fr_deputies.vert.7z
compilecorp fr_deputies --recompile-corpus
../deploy_to_beta.sh fr_deputies