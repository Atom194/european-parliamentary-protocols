#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp cz_deputies.reg /corpora/registry/cz_deputies

cat data/downloaded/* | $(ca_getpipeline cs) | 7zpipe /corpora/vert/parliament/cz_deputies.vert.7z
compilecorp cz_deputies --recompile-corpus
../deploy_to_beta.sh cz_deputies