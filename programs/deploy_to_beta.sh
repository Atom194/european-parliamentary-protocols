#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


deploy_to_beta $1
rsync /corpora/registry/$1 xmikusek@hyla1.sketchengine.eu:/corpora/registry_dev/$1