# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

from typing import Any, BinaryIO, List, Optional, TextIO
from crawler import Crawler, LOG_TYPE
from prevertbuilder import PrevertBuilder
import requests
import io
import shutil
import datetime
import re

API_KEY = None

remaining_legislations = []
remaining_urls = []


def process_url(crawler: Crawler,  url: str) -> Optional[str]:
    cycle, session, segment = re.sub(r'https://www\.parlament\.hu/cgi-bin/web-api-pub/felszolalas\.cgi\?access_token=.*&p_ckl=([0-9]+)&p_uln=([0-9]+)&p_felsz=([0-9]+)', "\\1 \\2 \\3", url).split()
    cycle = int(cycle)
    session = int(session)
    segment = int(segment)
    cursor_url_match = crawler.dbcursor.execute("SELECT url FROM downloaded WHERE url = ?", (url, ))
    if len(cursor_url_match.fetchall()) != 0:
        remaining_urls.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle, session, segment + 1))
        if segment == 1:
            remaining_urls.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle, session + 1, segment))
            if session == 1:
                remaining_legislations.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle + 1, session, segment))
                print(remaining_legislations)
        return None
    response = requests.get(url, timeout=1200)
    if not response.content.decode("utf-8").startswith('<?xml version="1.0" encoding="utf-8"?>\n<felszolalas '):
        return None
    crawler.log("Downloading: %s" % (url, ), LOG_TYPE.MESSAGE)
    remaining_urls.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle, session, segment + 1))
    if segment == 1:
        remaining_urls.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle, session + 1, segment))
        if session == 1:
            remaining_legislations.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=%s&p_uln=%s&p_felsz=%s" % (API_KEY, cycle + 1, session, segment))
            print(remaining_legislations)
    filename = "%s_%s_%s.prevert" % (cycle, session, segment)
    prevert_builder = PrevertBuilder(crawler, filename, doc_metadata={"source_url": "https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=&p_ckl=%s&p_uln=%s&p_felsz=%s" % (cycle + 1, session, segment), "url_access_time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC"), "filename": filename, "cycle": str(cycle), "session": str(session), "segment": str(segment)})
    prevert_builder.write(io.StringIO(response.content.decode("utf-8")))
    prevert_builder.build()
    return filename


def load_config(path_to_config) -> Any:
    import json
    try:
        with open(path_to_config, "r") as config_file:
            config = json.load(config_file)
    except FileNotFoundError as e:
        raise FileNotFoundError("File %s do not exist. Error: %s" % (path_to_config, str(e)))
    return config

def run(args: Any) -> None:
    global API_KEY
    config = load_config(args.config)
    crawler = Crawler(config["work_dir"], config["parlament_short_name"], config["smtp_servers"], config["mail_from"], config["mail_recepients"])
    crawler.start()
    if API_KEY is None:
        API_KEY = config["api_key"]
        cur = crawler.dbcursor.execute("SELECT url FROM downloaded ORDER BY id DESC LIMIT 1")
        last_record = cur.fetchall()
        if len(last_record[0]) == 0:
            remaining_legislations.append("https://www.parlament.hu/cgi-bin/web-api-pub/felszolalas.cgi?access_token=%s&p_ckl=34&p_uln=1&p_felsz=1" % (API_KEY, ))
        else:
            url = last_record[0][0]
            url = re.sub(r'&p_uln=[0-9]+&p_felsz=[0-9]+', "&p_uln=1&p_felsz=1", url)
            remaining_legislations.append(url)
    try:
        shutil.rmtree(crawler.dir_unconfirmed + "/*", ignore_errors=True)
        crawler.dbcursor.execute("CREATE TABLE IF NOT EXISTS downloaded (id INTEGER PRIMARY KEY ASC, date TEXT, url TEXT, file_name TEXT)")
        crawler.dbcursor.commit()

        while len(remaining_legislations) != 0:
            remaining_urls.append(remaining_legislations.pop(0))
            while len(remaining_urls) != 0:
                url = remaining_urls.pop(0)
                filename = process_url(crawler, url)
                if filename is not None:
                    crawler.dbcursor.execute("INSERT INTO downloaded (date, url, file_name) VALUES (?, ?, ?)", (datetime.datetime.utcnow().strftime("%Y-%m-%d"), url, filename))
                    crawler.dbcursor.commit()
    except:
        import traceback
        crawler.log(traceback.format_exc(), LOG_TYPE.ERROR)

    crawler.stop()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="./config.json", help="File where program config is stored.")

    args = parser.parse_args()
    run(args)

