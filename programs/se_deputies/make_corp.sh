#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License


cp se_deputies.reg /corpora/registry/se_deputies

cat data/downloaded/* | $(ca_getpipeline sv) | 7zpipe /corpora/vert/parliament/se_deputies.vert.7z
compilecorp se_deputies --recompile-corpus
../deploy_to_beta.sh se_deputies