#!/bin/bash
# Copyright 2023 Ota Mikušek
# This program is licensed under GNU Lesser General Public License

./zipper.sh
cp hu_deputies.reg /corpora/registry/hu_deputies

find data/downloaded/ | ./zipcat.py | $(ca_getpipeline hu) | 7zpipe /corpora/vert/parliament/hu_deputies.vert.7z
compilecorp hu_deputies --recompile-corpus
../deploy_to_beta.sh hu_deputies